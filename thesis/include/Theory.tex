\chapter{Theory}\label{ch:theory}
In this chapter the theoretical background necessary for our approach is presented. The chapter begins by an introduction of artificial neural networks and continues to introduce recurrent neural networks and significantly, long short-term memory. It then gives an introduction of language models and word embeddings. A method of measuring the distance between probability distributions (maximum mean discrepancy) is introduced. Finally, beam search, a variation of best-first-search is described.
 
\section{Artificial Neural networks}\label{sec:nn}
Artificial neural networks are, as can be imagined, inspired by the brain, consisting of interconnected neurons. It is however important to stress the inspired by. It is generally not true that artificial neural networks try to emulate neurons in the brain other than they consist of interconnected nodes (neurons). In the remainder of this thesis neural networks refers to artificial neural networks.

A neural network is basically a set of nodes (neurons) connected by weighted edges. The network nodes perform mathematical operations on the inputs and produces a non-linearity. In the figure \ref{fig:neuron-basic} a simple neuron is illustrated.

\begin{wrapfigure}{l}{0.5\textwidth}
\begin{tikzpicture}[shorten >=1pt, auto, node distance=3cm, ultra thick,
	edge_style/.style={draw=black, ultra thick},
	node_style/.style={circle, thick, draw=black!, minimum size = 30pt}]

\node[draw=white!] (x1) at (0,1) {$x_1$};
\node[draw=white!] (x2) at (0,0) {$x_2$};
\node[draw=white!] (x3) at (0,-1) {$x_3$};

\node[node_style] (a) at (2,0) {a};
\node[draw=white!] (y) at (4,0) {y};

\draw[->,ultra thick] (x1) edge node{} (a);
\draw[->,ultra thick] (x2) edge node{} (a);
\draw[->,ultra thick] (x3) edge node{} (a);

\draw[->,ultra thick] (a) edge node{} (y);
\end{tikzpicture}
\caption{Basic neuron}\label{fig:neuron-basic}
\end{wrapfigure}

In order to compute the output (y), the neuron calculates a so-called pre-activation by taking the dot product of the input and connecting weights $a = \begin{pmatrix}x_1,x_2,x_3\end{pmatrix} \begin{pmatrix}w_1\\w_2\\w_3\end{pmatrix}$. The output is then computed by applying an activation function to the pre-activation, usually $sigmoid(a)$ or $tanh(a)$ (figure \ref{fig:tanh-sigm}).  Applying the non-linearity forces the output within a fixed range.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figure/sigm_tanh}
	\caption{Plot for the logistic sigmoid and tanh functions.}
	\label{fig:tanh-sigm}
\end{figure}

The purpose of a neural network is to learn patterns, based on all data that it has seen. The learning process can be seen as trying to create an approximation of the real pattern. In order to do this it needs to know how good its approximations are, which is why it needs some \textit{error function}, commonly referred to as the \textit{loss} or \textit{cost} function.

There are many network architectures and types of neurons but for the purpose of this thesis we will focus on the recurrent neural network architecture and the long-short term memory cell.

\subsection{Recurrent Neural Networks}\label{sec:rnn}
Recurrent neural networks, in their basic form are simply neurons where the hidden unit is connected to itself. The purpose of this is that it allows for neurons to have a memory of the previous time steps. In figure \ref{fig:RNN-unrolled} a basic RNN is shown and unfolded for t, time steps.

\begin{figure}[h]
	\centering
	\input{figure/rnn-unrolled.tkiz}
	\caption{\textbf{Left:} Conceptual RNN model. \textbf{Right:} Unrolled RNN, for t time steps.}\label{fig:RNN-unrolled}
\end{figure}

The equation for computing the output signal $h_t$ at a time t is shown in equation \ref{eq:RNN}, where $W^{(hh)}$ is the weight between the neurons in the hidden layer and $W^{(hx)}$ the weight between the hidden layer and the input.
\begin{align}\label{eq:RNN}
h_t = \sigma (W^{(hh)}h_{t-1}+W^{(hx)}x_t)
\end{align}

Such, so-called vanilla RNNs were previously disregarded because they did not work well in practice. This was because they were difficult to train and did not preserve a memory over long time periods. The reason for this is that every network has a loss function which it tries to minimize. The network weights are updated by computing gradient of the loss function and performing some gradient descent based algorithm. The problem with vanilla RNNs is that this gradient will either diverge towards infinity or converge towards zero over time. This results in large parameter updates, meaning large changes at each time step, they are therefore bad at holding long-term dependencies. This problem is called the exploding or vanishing gradient. There have been many attempts in order to solve this problem. Some approaches include using rectified linear neurons, scaling and clipping of the gradient if it exceeds some constant threshold. The problem of vanishing gradients is more difficult to address.

The problem of vanishing and exploding gradients has lead to the development of new, more complex neurons. There are some variations but common for these neurons is that they introduce multiplicative interactions between the hidden states. The most common types of neurons which are free from the exploding gradient problem are gated recurrent units (GRU) and long short-term memory (LSTM). They have both shown very good results in holding long-term dependencies \citep{chung2014empirical}. For the purpose of this thesis we will only discuss LSTMs but it could be argued that GRUs are a special case of LSTMs.

\subsection{Long Short-Term Memory Neural Networks}
As previously discussed, vanilla RNNs have the problem that it is difficult to model long term dependencies because of the vanishing or exploding gradient problem. This lead to the introduction of the LSTM cell \citep{hochreiter1997long} and the addition of the forget gate \citep{gers2000learning}. LSTM neural networks are artificial neural networks consisting of LSTM (Long term Short term Memory) cells. Using LSTMs (or GRUs) is a way of improving the RNNs capabilities of modelling long-term and short-term dependencies by introducing gated multiplicative interaction between the hidden states. The idea is that multiplicative interactions determine if and how much of the input and previous state should affect the current state of the neuron. Conceptually the LSTM cells are based on the idea that neurons have memory and that they forget while learning. Figure \ref{fig:lstm} illustrates a common variant of the LSTM cell \citep{graves2013generating}.

\begin{figure}[h]
\centering
\input{figure/lstm.tkiz}
\caption{Generating sequences with recurrent neural networks. \citep{graves2013generating}}\label{fig:lstm}
\end{figure}

The components of the LSTM are the \textit{input gate}, \textit{cell state}, \textit{output gate} and \textit{forget gate}. Firstly, the \textit{cell state} ($c_t$) is essentially acting as the neuron memory which together with the \textit{forget gate} ($f_t$) determines how much of the cell state of the previous state should be preserved. The \textit{input gate} ($i_t$) determines what part of the input should be part of the new cell state. The current cell state is then updated based on the filtered input and cell state ($c_t = f_t \otimes c_{t-1} + i_t \otimes j_t$). The output gate of the neuron decides what part of the new cell state it should output. 

The mathematical description of the summary above is stated in the system of equations below. Here the calculation of $h_t$ is clearly much more involved than in \ref{eq:RNN}.

% TODO: check this, is does it correspond to the implementation?
\begin{align}
    i_t &= \tanh(W^{(xi)}x_t + W^{(hi)}h_{t-1} + b_i) \label{eq:lstm-i} \\
    j_t &= \sigma(W^{(xj)}x_t + W^{(hj)}h_{t-1} + b_j)\label{eq:lstm-j} \\
    f_t &= \sigma(W^{(xf)}x_t + W^{(hf)}h_{t-1} + b_f) \label{eq:lstm-f} \\
    o_t &= \sigma(W^{(xo)}x_t + W^{(ho)}h_{t-1} + b_o) \label{eq:lstm-o}\\
    c_t &= f_t \otimes c_{t-1} + i_t \otimes j_t \label{eq:lstm-c} \\
    h_t &= \tanh(c_t) \otimes o_t 
\end{align}

As mentioned above this is one of many variations of LSTM which is used by \citep{graves2013generating}. \cite{greff2015lstm}  performed a large evaluation of the vanilla LSTM architecture and eight other popular architectures on acoustic modelling, hand writing recognition and polyphonic music modelling. They found that the different variations did not significantly improve upon the vanilla LSTM.

\subsection{Backpropagation throught time}\label{sec:BTT}
The weights between the nodes in the neural network are updated, often referred to as the network is learning. In order to update the weights the error will have to be propagate from the output and back to the preceding neurons. For recurrent neural networks the algorithm for updating the weights is called backpropagation through time (BPTT) and is essentially the backpropagation algorithm applied to an unrolled recurrent neural network (discussed in section \ref{sec:rnn}). The key difference is that the weight matrices are summed over each step in the unrolled RNN which is the cause of the exploding and vanishing gradient problem discussed in \ref{sec:rnn}.

For long sequences BPTT is expensive to compute, thus it is common to not compute the full BPTT but instead stop after a number of time steps. This is referred to as truncated BPTT and can be seen as an approximation of BPTT.

\subsection{Cross entropy error}\label{sec:cross-entropy-error}
As discussed in section \ref{sec:nn} neural networks has a loss function. The goal of the network is to minimize that loss function. This means that an appropriate loss function has to be determined.

For back-propagation to train the neural network to estimate the objective function it is necessary that a loss function is defined. Usually, for recurrent neural networks the cross entropy loss function (defined in equation \ref{eq:cross-entropy-error}) is used.

\begin{align}\label{eq:cross-entropy-error}
C= -\frac{1}{N} \sum_{i=1}^{N} [t_i ln(y_i) + (1-t_i)ln(1-y_i)]
\end{align}

Intuitively, the loss function is computing the average of all cross entropies for the N examples as in equation \ref{eq:cross-entropy-error}. Here $t_i$ is the target and $y_i$ the prediction made by the network.

\subsection{Dropout}
Dropout is a regularization technique that stochastically remove hidden units of the neural network. Practically this means that all outgoing connections from the hidden unit are zero and does not affect the rest of the network. This also means that no error is back-propagated through the removed hidden unit. The reason that it is zeroed out is that it is computationally practical as the computations in neural networks corresponds to matrix multiplications. Dropout has been shown to be successful for regularizing LSTM neural network and reduce overfitting \citep{zaremba2014recurrent}, increasing the performance of the network.

\section{Statistical Language models}
Statistical language models are very useful in natural language processing as they provide a way of comparing the probability of different sentences. This means that given a set of words, forming a sentence $S =\{w_1,w_2,...,w_n\}$ a language model can be used to determine $P(S)$. Statistical language models therefore also provides the possibility to determine probability of the next word in a sentence by computing $P(w_m | S)$. This is useful in many applications, for instance spell checking, machine translation and speech recognition. All the mentioned applications will need to determine and compare the relative probabilities of sentences in order to choose the best one. In the remainder of this thesis language models refers to statistical language models.

There are many types of language models but the most commonly used are the n-gram, skip-gram and neural network models. For the purposes of this thesis the neural network language models are most important as it is what is used in our approach but a brief introduction to some competing models are given.

\subsection{n-gram models}
N-gram models depend on count-based statistics based on the frequency at which a set of words exist together. The n in n-gram stands for the sequence length, so 2-gram (bi-gram) would assign a probability of any 2-tuple $(w_1,w_2)$ that it has seen in the text based on how frequently they occur together. This means that the bi-gram can predict the next word by looking at the previous word. 

Formally, for the bi-gram model the probability that a word $w_{i-1}$ is followed by $w_i$ is determined by the number of times this occurs in the dataset, normalized by the number of times the word $w_{i-1}$ occurs in any context. $$Pr(w_i|w_{i-1})=\frac{count(w_{i-1},w_i)}{count(w_{i-1})}$$

\subsection{skip-gram models}
Skip-gram models are a generalization of n-gram models where a fixed number of words are skipped when creating the statistics. To understand why it is a generalization, a better definition of skip grams could be n-skip-n-gram. \\

\noindent For instance, the difference between 0-skip-bi-gram and 1-skip-bi-gram for the sentence "The cat sat on the mat" is shown below.\\

\noindent\textbf{0-skip bigram} : Equivalent to the bi-gram forming the following bi-grams: "the cat", "cat sat", "sat on", "on the", "the mat". \\
\noindent\textbf{1-skip bigram} : Forms the following bigrams: "The cat", "the sat", "cat sat", "cat on", "sat on", "sat the", "on the", "on mat", "the mat".\\

\noindent This means that skip-grams include more data about the context of each word than n-grams. \cite{guthrie2006closer} show that using skip-grams can be more effective than simply increasing the dataset for the n-gram models at the cost of computation.

\subsection{Neural network models}
As for the count based models, there are many variations of the neural language models. For the purposes of this thesis only a LSTM neural language model will be described.
Recurrent neural networks and in particular LSTM neural networks have been very successfully used in language modelling \citep{sundermeyer2012lstm}. A LSTM language model is often simply a multilayered LSTM. Figure \ref{fig:RNN-unrolled} shows an example recurrent network architecture which could describe a recurrent language model. At each time step it tries to predict the next word in the input sequence given the previous words.

It corresponds to a multi-class classification over the entire vocabulary. Therefore, it is using a softmax (equation \ref{eq:softmax}) layer which basically forces the sum of all output predictions to equal one converting it to a probability distribution of the next word in the vocabulary.

\begin{equation}\label{eq:softmax}
P(y|x) = \prod_{t=1}^{n}\frac{exp(f(h_{t-1},x_t))}{\sum_{t' \neq t}f(h_{t-1},x_{t'})}
\end{equation}

Neural language models have an advantage over count based models in that they are able to generalize outside of the examples it has already seen and model long-term relationships. The count based models depend on the examples that already exist in the dataset and they can not predict (count) what they have not seen. 

\section{Word Embeddings}
Natural language processing is concerned with modelling and understanding language. In order to apply machine learning to a sequence of words, making up a book, novel or review, the sentences have to be represented by numerical values. The straight forward way of doing this is to randomly or incrementally assigning a number to each unique word. This approach is commonly used because of its simplicity but results in a high data sparsity. Additionally, the numerical value assigned to each word encodes no more information than a lookup id, used for reconstructing the text.

Another approach is to create and train what is called word embeddings. Word embeddings are vectorial representations of words in a so-called word-vector space. This means they represent a mapping $W \rightarrow \mathbb{R}^n$. The idea is that, representing words in the word-vector space means that the vectors can be updated by training and that the vectors are in this way given some contextual information. There are many implementations but usually, the word vectors of similar words are close in space. Exceptions include image caption generation where the colours red and yellow should not be close in space as it is likely that the model would choose the wrong colour. An interesting method for creating good word embeddings is called word2vec \cite{mikolov2014word2vec}. The resulting word embedding is such that mathematical operations as the following; $king - man + woman \approx queen$ can be performed. This is clearly a more promising way of representing words, as it holds more semantic meaning than simply unique numbers. By using t-SNE it is possible to visualize the word embeddings in 2d-space as shown in figure \ref{fig:word2vec-tsne}.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{figure/tsne}
	\caption{Words are represented by n-dimensional vectors and t-SNE is used to visualize them in two dimensions. Similar words are close in the map created by t-SNE.}\label{fig:word2vec-tsne}
\end{figure}

There are many approaches to create word-embeddings including n-gram, neural network models, word2vec, convolutional models. For the purposes of this thesis only a neural network method will be covered.

\subsection{Neural Network Language Method}
The way that a word embedding can be created based a neural network is that we build a vocabulary of unique words from a dataset. The word embedding of each word in the vocabulary is simply a high dimensional vector which is initialized. Initialization could be by using some pre trained embeddings but is often simply sampled from a uniformly random distribution. The input words to the neural network are encoded into embeddings which is used as input to the first layer. When the neural network is then trained and the error is back-propagated through the network the word embedding is also updated based on the error. This means that the embedding is trained to improve the rest of the network. When the neural network is a language model, trying to predict the next word in a sequence, the resulting embedding will have the similar characteristics as the embedding visualized in figure \ref{fig:word2vec-tsne} where similar words are grouped together.

\section{Sequence-to-sequence models}\label{sec:seq2seq}
A sequence-to-sequence model consist of two components, an encoder and a decoder. The model is trained with an input sequence $X = x_1,x_2,...,x_n$ paired with an output sequence $Y = y_1,y_2,...,y_n$. The models objective is to predict $Y$ given $X$. This is done by having the encoder encode the input sequence into its state matrix. The decoder is then give the state matrix of the encoder and tries to decode the output sequence.

Figure \ref{fig:seq2seq} illustrates a sequence to sequence mode where the encoder and decoders are unfolded for a number of time steps. The encoder gets a sequence of inputs ($A,B,C,<eos>$) and creates an encoding (internal representation). The encoding is then used by the decoder to generate one output at a time ($W,X,Y,Z,<eos>$), passing the generated output as input to the next state. Both the encoder and decoder can be implemented simply by using two separate single or multilayered LSTMs.
\begin{figure}[h!]
\input{figure/seq2seq.tkiz}
\caption{Sequence-to-sequence model}\label{fig:seq2seq}
\end{figure}

Sequence-to-sequence models are very popular and have been successfully used for a wide variety of learning problems, for instance machine translation and image captioning but also classification which can modelled as translating the input sequence into a binary language \citep{sutskever2014sequence}.

\section{Maximum Mean Discrepancy}\label{sec:mmd}
Maximum mean discrepancy (MMD) \citep{gretton2006kernel} is a method for measuring distances between probability distributions. MMD provide us with a method to determine which distribution a given sample most likely belongs to. In our approach, roughly, this is the method that guides the transformation of the input sequence to the output sequence.

Formally, maximum mean discrepancy (equation \ref{eq:mmd}) is a statistic used to solve the two sample problem: Given two i.i.d samples $X = \{x_1,...,x_n\} \sim p$ and $Y = \{y_1,...,y_m\}\sim q$ it determines if $p = q$. Where $P^s$ is the probability distribution of the source and $P^t$ of the target.

\begin{equation}\label{eq:mmd}
MMD(P^s,P^t,\mathcal{F})=\sup_{f\in \mathcal{F}}\left(\E[f(\mathbf{z}^s)]_{\mathbf{z}^s \sim P^s} - \E[f(\mathbf{z}^t)]_{\mathbf{z}^t \sim P^t}\right)
\end{equation}

Given that $\mathcal{F}$ is a reproducing kernel Hilbert space (RHKS) and $k$ is a Gaussian kernel function the witness function $f^*(\mathbf{z})$ \ref{eq:witness-function} maximizes the difference expressed by the statistic. This function can therefore be used to determine if the sample $\mathbf{z}$ is a sample belonging to $P^s$ or $P^t$.

\begin{equation}\label{eq:witness-function}
f^*(\mathbf{z}) = \E{(k(\mathbf{z}^s,\mathbf{z}))}_{\mathbf{z}^s\sim P^s} - \E{(k(\mathbf{z}^t,\mathbf{z}))}_{\mathbf{z}^t\sim P^t}\approx \frac{1}{m}\sum_{i=1}^{m}k(\mathbf{z}^s,\mathbf{z})-\frac{1}{n}\sum_{i=1}^{n}k(\mathbf{z}^t,\mathbf{z})
\end{equation}

The witness function will be positive if the sample ($\mathbf{z}$) belongs to the source class ($s$) and negative target class ($t$).

\section{TensorFlow}
TensorFlow is a open source library for machine learning developed by Google and publicly released in 2015. Tensorflow has support for the common architectures, optimizers and cells. 

The API is written in python and is based on the concept of defining graphs that represent neural networks. The graphs consist of nodes, which are mathematical operations and connections, represented by tensors, (multi-dimensional arrays). Back-propagation is easily implemented by defining a loss function to optimize and use one of a number of implemented back-propagation algorithms, for instance stochastic gradient descent.

Tensorflow is mostly implemented with GPU support which is essential for training deep neural networks.

\section{Beam search}\label{sec:beam-search}
The Beam Search algorithm heuristic search algorithm which is a variation of the best-first search algorithm. The idea is that instead of using a greedy strategy, always choosing the locally optimal solution the algorithm explore a number of locally optimal solutions. The number of candidate solutions corresponds to the beam size. At each iteration it will evaluate if the selections it has made are optimal or if there are other better selections available. If the search space is modelled as a tree, then a solution corresponds to a path in the tree. Beam search will at each level in the tree, keep the b optimal paths up until and including the nodes in that level.

Beam search is clearly an approximation algorithm as it has no guarantees of finding the optimal solution in a search space but it is always at leas as good as the greedy algorithm since the greedy algorithm corresponds to beam search with a beam size of one. Beam search is therefore suitable in situations where the search space is very large and thus exhaustive search is infeasible. For instance, in machine translation, between two languages this is usually the case. When translating English to French, the language model will be used to indicate which word is the most probable next word in a translated sentence, but greedy selection of the most probable word may not yield the most probable sequence. Beam search allows for the exploration multiple candidate solutions.

Examples of how beam search has been used for machine translation can be found in \cite{koehn2004pharaoh} and more recently \citep{sutskever2014sequence}. Beam search has however been applied to a wide variety of problems. For the purposes of this thesis the most interesting application is however on sequence to sequence models as described in \citep{sutskever2014sequence} where it is used for the decoding.