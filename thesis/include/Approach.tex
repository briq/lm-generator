% CREATED BY DAVID FRISK, 2015
\chapter{Approach}\label{ch:methods}
In this chapter the methods used are described using the theoretical framework presented in chapter \ref{ch:theory}. The chapter begins with giving an overview of the system, and then breaks it down to three steps; manifold representation, manifold traversal and output reconstruction.

\section{Model overview}
Generally, the goal of the system is to be able to train on two different sentiments, a source sentiment and target sentiment. Given some input belonging to the source sentiment, the system should be able to generate output of the target sentiment, while maintaining the information content by changing deep features of the input. 

The model that was employed for generating conceptually transformed text can be partitioned into three steps; manifold representation, manifold traversal and output reconstruction. The system follows the pattern of sequence-to-sequence models discussed in section \ref{sec:seq2seq}. It primarily consists of two components, encoder and decoder with maximum mean discrepancy between the components, as depicted in figure \ref{fig:system-overview}.

\begin{figure}
\centering
\begin{tikzpicture}[shorten >=1pt, auto, node distance=3cm, ultra thick,
	edge_style/.style={draw=black, ultra thick},
	node_style/.style={circle, thick, draw=white!, minimum size = 30pt},
	input_style/.style={circle, thick, draw=white!, minimum size = 15pt}]
	
	\node[node_style] (x) at (0,0) {$x$};
	\node[node_style] (tx) at (4,0) {$\phi(\mathbf{x})$};
	\node[node_style] (tpx) at (4,-3) {$\phi'(\mathbf{x})$};
	\node[node_style] (xp) at (0,-3) {$x'$};
	
	\draw[->,edge_style] (x) edge node{$LM_1$} (tx);
	\draw[->,edge_style] (tx) edge node{$MMD$} (tpx);
	\draw[->,edge_style] (tpx) edge node{$LM_2$} (xp);
	
\end{tikzpicture}
\caption{Schematic representation of the system}
\label{fig:system-overview}
\end{figure}

Briefly summarized, the model is given a set of source and target sentences, each consisting of a sequence of words. The input sentences $\mathbf{x}_1^s,...,\mathbf{x}_m^s$ belongs to a source class $y^s$, the output sentences $\mathbf{x}_1^t,...,\mathbf{x}_n^t$ to a target class $y^t$. The source-target class tuple could for instance be postitive negative or formal, informal. The goal is to train the model using examples from the source and target class, given an input ($\mathbf{x}$) of the source class it should be able to generate output ($\mathbf{x'}$) of the target class by changing the deep features of $\mathbf{x}$. Formally the transformation is denoted by $\mathbf{x} \rightarrow \mathbf{x'}$. 

The first step (section \ref{sec:manifold-representation}) in the process of generating the output involves processing of the input sentences, $\mathbf{x}_1^s,...,\mathbf{x}_m^s$ belonging to the source class and embedding them on a manifold, meaning that we create an encoding of $\mathbf{x}$, $\phi(\mathbf{x})$. This corresponds to $\mathbf{x} \rightarrow \phi(\mathbf{x})$.

The goal of the second step (section \ref{sec:manifold-traversal}) is to transform $\phi(\mathbf{x}) \rightarrow \phi'(\mathbf{x})$ where $\phi'(\mathbf{x})$ is classified as the target class. The transformation is guided by regularized maximum mean discrepancy to make small modifications to $\phi(\mathbf{x})$, yet transforming it into the target class. This corresponds to $\phi(\mathbf{x}) \rightarrow \phi'(\mathbf{x})$. 

The final step (section \ref{sec:output-reconstruction}) reconstructs the output, meaning the conceptually transformed text. This is done by using another language model and beam search in order to find a good output sentence belonging to the target class. This corresponds to $\phi'(\mathbf{x}) \rightarrow \mathbf{x'}$.

As mentioned earlier, the model described above belongs to the category of generative and sequence-to-sequence models. The way in which it differs from most sequence-to-sequence models is that there is no sequence-to-sequence data-set to train on. What this means is that for a given word or sentence, the model has knowledge of the probability distributions $P^s$ and $P^t$. It however has no knowledge about the correct, reconstructed output sequence as it has no input, output tuples to verify its predictions. In the following sections the respective model parts will be discussed in further detail.

\section{Manifold representation}\label{sec:manifold-representation}
The first step of the model is to represent $\mathbf{x}$ on the manifold, this corresponds to the encoding of the input sequence. The encoding $\mathbf{x} \rightarrow \phi(\mathbf{x})$ is computed by a language model (figure \ref{fig:LM-unrolled}). The language model consists of a multi-layered LSTM cell, which is given one input at each time step. As for most sequence-to-sequence models, the idea is that at each time step the language model tries to predict the next word in the sentence given the previous states and input. This means at each time step the LSTM state should encode the previous words, as this is what determines its posterior distribution $p(x_t|x_1,...,x_{t-1})$. The loss function which the language model tries to minimize is the cross entropy loss function as described in equation \ref{eq:cross-entropy-error}.

\begin{equation}\label{eq:cross-entropy-error}
loss = - \frac{1}{N} \sum_{i=1}^{N} ln(p_{target_i})
\end{equation}

\begin{figure}[h]
	\centering
	\input{figure/lstm_lm.tkiz}
	\caption{\textbf{Left:} Reconnected language model. \textbf{Right:} Unrolled language model, for t time steps.}\label{fig:LM-unrolled}
\end{figure}

In order to encode the input sentences the language model has been trained in two separate steps. First on a largely neutral corpus. This results in an embedding where semantically similar words are close to each other on the manifold. The language model is then biased towards sentiment classification, which intends to slightly change the word embedding to cluster together words that are of the same sentiment. The idea is that if the language model had been trained simply on the sentiment classification data it would simply cluster words that are of the same sentiment but may not me semantically similar. More details of the implementation and training is described in section \ref{sec:training}.

The reason for encoding the input sequence is that if the model can use a linear classifier in $\phi$ to determine the sentiment it should also be able to interpolate in $\phi$ without leaving the manifold.
	
\section{Manifold traversal}\label{sec:manifold-traversal}
The manifold traversal is where we try to move the encoding produced by the biased language model ($\phi(\mathbf{x})$) to a subspace where it is likely classified as the target sentiment ($\phi(\mathbf{x}) \rightarrow \phi'(\mathbf{x})$). The traversal is guided by maximum mean discrepancy (MMD), introduced in section \ref{sec:mmd}. This corresponds to using the witness function together with a regularizer, introducing resistance to change.

The witness function was introduced in section \ref{sec:mmd}. Here $\mathbf{x}$ is the input to the system and $\mathbf{z} = \phi(\mathbf{x})$. The variables $\mathbf{z}^s = \mathbf{z}_1^s,...,\mathbf{z}_m^s$ and $\mathbf{z}^t = \mathbf{z}_1^t,...,\mathbf{z}_n^t$ are samples from the source and target distributions ($P^s$, $P^t)$ of size m and n.

\begin{equation}
f^*(\mathbf{z}) \approx \frac{1}{m}\sum_{i=1}^{m}k(\mathbf{z}^s_i,\mathbf{z})-\frac{1}{n}\sum_{i=1}^{n}k(\mathbf{z}^t_i,\mathbf{z})
\end{equation}

If we would simply use the witness function as optimization loss without any resistance it would move $\phi(\mathbf{x})$ to far on the manifold and not preserve much of the information content of the text \citep{gardner2015deep}. The idea is that the system should instead only change the deep features and preserve as much of the information content as possible. Therefore, a budget of change was introduced. The function we want to minimize is defined in equation \ref{eq:reg-mmd}.

\begin{align}\label{eq:reg-mmd}
\mathbf{z}^t &= \mathbf{z}^s + \bm{\delta} \\ 
\bm{\delta} &= \argmin_{\bm{\delta}} f^*(\mathbf{z}^s+\bm{\delta}) + \lambda ||\bm{\delta}||_2^2
\end{align}
	
Here the kernel function used in this thesis is a Gaussian, defined in equation \ref{eq:gaussian-kernel}.

\begin{equation}\label{eq:gaussian-kernel}
k(z,z')=e^{\frac{1}{2\sigma}|\mathbf{z}-\mathbf{z'}|^2}
\end{equation}	

\noindent The samples $\mathbf{z}^s$ and $\mathbf{z}^t$ are computed by the encoder described in the previous section.

\section{Output reconstruction}\label{sec:output-reconstruction}
The output reconstruction part of the systems is concerned with transforming $\phi'(\mathbf{x}) \rightarrow \mathbf{x}'$. This reconstruction is guided by a second language model and optimized using algorithm called beam search (section \ref{sec:beam-search}).

The second language model is a decoder neural network consisting of a multilayered LSTM. The way the decoder is used in our model is that it is given the last state of the first language model, which has been transformed by regularized maximum mean discrepancy. The objective of the decoder model is then to reconstruct the output given this encoding. If that encoding belongs to the target class $y_t$, then the reconstructed output should also be of the target class $y^t$.

The encoder-decoder network is shown in figure \ref{fig:sequence-to-sequence-mmd}. If the MMD transformation corresponds to applying some function to $\mathbf{z}$,
$g(\mathbf{z})$ then we want $g(LM_1(\mathbf{x}))=LM_2^{-1}(\mathbf{x}')$.

\begin{figure}[H]
\input{figure/seq2seq-generator.tkiz}
\caption{Sequence-to-sequence model where the first n states corresponds to $LM_1$ the last n states $LM_2$. The connection between them, regularized maximum mean discrepancy. }\label{fig:sequence-to-sequence-mmd}
\end{figure}

\subsection{Selecting output sentence}
The language model is concerned with predicting the probability distribution of the next word in the sentence, thus it is estimating the probability of a sequence. In order to reconstruct the output sequence, at each step an algorithm would have to select one of the words in the vocabulary based on this probability distribution. The straight forward approach is to have a greedy algorithm, simply selecting the word with the highest probability. However, we have used another approach called beam search, introduced in section \ref{sec:beam-search}. What it means is that at each time step in the decoder part of the generator, the $b$ most probable words are selected, where b is our so called beam size. This is propagated through the network and requires more computation but allows us to keep candidate solutions that are not locally optimal but in the end give a higher sequence probability. 

The output of the language models is a probability distribution over the entire vocabulary. The greedy approach would simply be to perform an argmax over the logits. What is done instead is that argmax is performed but $b$ candidate solutions are kept.

\section{Implementation}
The following sections discuss the implementation and training of the model in more detail.

\subsection{Training of the language model}\label{sec:training}
As discussed above the model consist of two components, the first and second language model. Additionally, during training a third component is introduced. This component is a sentiment classifier. The regularized maximum mean discrepancy is not used during training, only during generation.

The training is divided into three phases, training of the encoder, bias the encoder and finally train the entire network. In the first phase, the first language model is trained on the Penn Treebank corpus (section \ref{sec:ptb}). This is to train the encoder on the task of modelling largely neutral language. In the second phase, the encoder is trained on the UMICH dataset (section \ref{sec:umich}) together with a binary classifier network. In the last phase both the language models are trained together as an autoencoder, trying to predict the input.

In the first phase the goal of the encoder is simply, given a sequence of words, predict the next word in the sequence. This is done by dividing the PTB data into mini-batches. The target is the input shifted by one time step. The training is done using truncated back-propagation through time (section \ref{sec:BTT}). This means that it is looking only at a specified number of words in the sequence. The cross entropy loss function that we employ is the negative log likelihood of the target word based on the output from the encoder (section \ref{sec:cross-entropy-error}). Parameters are updated using the Adam optimizer \citep{kingma2014adam} which is a type of stochastic gradient descent optimizer.

In the second phase the language model is trained using a UMICH dataset which consists of highly polar positive and negative sentences (section \ref{sec:datasets}). A classifier (figure \ref{fig:classifier-network}) is added on to the network, which attempts to bias the language model towards sentiment classification. The classifier connects the LSTMs to a mean pooling layer, averaging over the LSTM states for the truncated back-propagation. Then it preforms logistic regression over the pooled state in order to perform the classification of the sentences. In this phase the same optimizer is used but the encoder has a lower learning rate as it is pre-trained. The loss function that is optimized is the classification loss. 

\begin{figure}[H]
	\centering
	\input{figure/classifier-network.tkiz}
	\caption{The classifier network}\label{fig:classifier-network}
\end{figure}

In the final phase of the training, the biased encoder and the decoder is trained together as an autoencoder network, trying to predict the input. The error gradients are thus based on the decoder loss and backpropagated to the encoder and word embeddings.

\subsection{Input Vectorization}\label{sec:inputvectorization}
The input text is tokenized (split) into words where each word is represented by assigning it a unique identifier. The identifier is presciently stored so that the word identifiers do not change when the model is restored or different for the datasets. Using Tensorflow, the matrices containing a number of input sentences are embedded into a word embedding which associates a high dimensional vector to each word.

\subsection{Word embedding}\label{sec:word-embeddning-implement}
A word-embedding is created by initializing the word-vectors with a uniformly distributed random value in the range [-1,1]. The error of the loss function is then back-propagated also to the word embeddings which are thus updated (trained).

\subsection{Regularization}
The LSTM cells are regularized using dropout regularization. This allows for the model to zero out certain units in the network. This was done by assigning a keep probability to the LSTM model which then randomly zero out units based on the probability that the units should be kept. The advanced LSTM model in Tensorflow was used as it has support for setting the keep probability.

\section{Datasets}\label{sec:datasets}
The following sections contain summaries of the datasets that were used.

\subsection{Penn Treebank Dataset}\label{sec:ptb}
For training the language model two datasets are used. The first data set is the Penn Treebank dataset. This dataset is a large corpus consisting of over 4.5 million words of largely neutral American English gathered from news articles. The data was pre-processed and divided to 887521 words of training examples, 78669 words of validation examples and 70390 words of testing examples.

\subsection{UMICH dataset}\label{sec:umich}
The UMICH dataset consists of labelled single sentence data downloaded from opinmind.com. Each sentence is hand-labelled based on its sentiment polarity, it is labelled zero if negative and one if positive. The training and test data was divided into 5669 sentences of training examples and 1417 sentences for testing examples. 