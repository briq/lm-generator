# Copyright 2015 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

# pylint: disable=unused-import,g-bad-import-order

"""Utilities for parsing PTB text files."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import gzip
import re
import tarfile

from six.moves import urllib

from tensorflow.python.platform import gfile
import collections
import os
import sys
import time

import nltk

import random

import tensorflow.python.platform

import numpy as np
np.set_printoptions(threshold=np.nan)

from six.moves import xrange  # pylint: disable=redefined-builtin
import tensorflow as tf

from tensorflow.python.platform import gfile

id_to_word = dict()

# Special vocabulary symbols - we always put them at the start.
_PAD = b"<PAD>"
_GO = b"<GO>"
_EOS = b"<EOS>"
_UNK = b"<unk>"
_START_VOCAB = [_PAD, _GO, _EOS, _UNK]

PAD_ID = 0
GO_ID = 1
EOS_ID = 2
UNK_ID = 3

# Regular expressions used to tokenize.
_WORD_SPLIT = re.compile(b"([.,!?\"':;)(])")
_DIGIT_RE = re.compile(br"\d")

def basic_tokenizer(sentence):
  """Very basic tokenizer: split the sentence into a list of tokens."""
  words = []
  for space_separated_fragment in sentence.strip().split():
    words.extend(re.split(_WORD_SPLIT, space_separated_fragment))

  #words = nltk.word_tokenize(sentence.decode('utf-8'))
  return [w for w in words if w]
  #return [x.encode('utf-8') for x in words]

def read_to_int_array(path):
    with gfile.GFile(path, "r") as f:
        sentences = list()
        for line in f.read().splitlines():
            sentences.extend(list(map(lambda word: int(word), line.split())))
    return sentences

def umich_raw_data(batch_size, vocabulary, data_path=None):
    """Load UMICH SI650 - Sentiment Classification raw data from data
     directory "data_path".

    Reads UMICH SI650 - Sentiment Classification text files, converts
    strings to integer ids, and performs mini-batching of the inputs.

    The UMICH SI650 - Sentiment Classification dataset comes from:

    https://inclass.kaggle.com/c/si650winter11/data

    Args:
        data_path: string path to the directory where umic.traing.txt and
        umic.test.txt are stored

    Returns:
        tuple (train_data, valid_data, test_data, vocabulary)
        where each of the data objects can be passed to PTBIterator.
    """
    data_sentence_path  = os.path.join(data_path, "processed", "umich.train.ids")
    data_target_path    = os.path.join(data_path, "umich.train.targets.txt")

    sentences = read_to_int_array(data_sentence_path)

    with gfile.GFile(data_target_path, "r") as f:
        targets = list(map(
            lambda x: [1, 0] if x == "0" else [0, 1],
            f.read().splitlines()
        ))


    test_data       = list()
    validation_data = list()

    # Randomly sample 80% to the traing set and 20% to the test set
    data_len = len(sentences)
    batch_len = data_len // batch_size

    data = np.zeros([batch_len,batch_size], dtype=np.int32)

    for i in range(batch_len):
        data[i] = sentences[i*batch_size:batch_size * (i + 1)]

    train_part = int(0.8*batch_len)

    # Randomly generate unique indeces
    train_idices = random.sample(range(0, batch_len), train_part)

    # Select training data
    train_data      = list()
    train_targets   = list()
    try:
        for ix in train_idices:
            train_data += list(data[ix,:])
            train_targets.append(targets[ix])
    except Exception as e:
        print(ix)

    # Select test/validation data
    test_data       = list()
    test_targets    = list()
    for ix in range(batch_len):
        if not ix in train_idices:
            test_data += list(data[ix,:])
            test_targets.append(targets[ix])

    return train_data, test_data, vocabulary, train_targets, test_targets

def _read_words_umich(filename):
    with gfile.GFile(filename, "r") as f:
        corpus = b""
        targets = list()

        for example in f.read().splitlines():

            try:
                label, text = example.split('\t')
            except:
                label, text = (-1, example)

            target = [0, 0]
            target[int(label)] = 1
            targets.append(target)
            corpus = corpus + _padding(text.encode('utf-8'))

    return basic_tokenizer(corpus), targets

def _padding(text, length=49, padding_word=b" "+ _PAD + b" ", tokenizer=basic_tokenizer):
    padding_symbols = (padding_word)*(length-len(tokenizer(text)))
    return text + b" " + _EOS + b" " + padding_symbols

def _read_words_ptb(filename, tokenizer=basic_tokenizer):
    with gfile.GFile(filename, "r") as f:
        return tokenizer(f.read().replace("\n", " <EOS> ").encode('utf-8'))


def _build_vocab(_read_words_func, filename):
    try:
        data, targets = _read_words_func(filename)
    except ValueError:
        data   = _read_words_func(filename)
        targets = list()

    counter = collections.Counter(data)
    count_pairs = sorted(counter.items(), key=lambda x: (-x[1], x[0]))

    words, _ = list(zip(*count_pairs))
    word_to_id = dict(zip(words, range(len(words))))
    id_to_word = dict(zip(range(len(words)),words))

    return word_to_id, id_to_word, targets


def _file_to_word_ids(_read_words_func, filename, word_to_id):
    try:
        data, targets = _read_words_func(filename)
    except ValueError:
        data   = _read_words_func(filename)
        targets = list()

    word_ids = []
    for word in data:
        try:
            word = word.encode('UTF-8')
            word_ids.append(word_to_id[word])
        except:
            word_ids.append(UNK_ID)
    return word_ids, targets


def ptb_raw_data(vocabulary, data_path=None):
    """Load PTB raw data from data directory "data_path".

    Reads PTB text files, converts strings to integer ids,
    and performs mini-batching of the inputs.

    The PTB dataset comes from Tomas Mikolov's webpage:

    http://www.fit.vutbr.cz/~imikolov/rnnlm/simple-examples.tgz

    Args:
      data_path: string path to the directory where simple-examples.tgz has
        been extracted.

    Returns:
      tuple (train_data, valid_data, test_data, vocabulary)
      where each of the data objects can be passed to PTBIterator.
    """
    global id_to_word

    train_path = os.path.join(data_path, "processed", "ptb.train.ids")
    valid_path = os.path.join(data_path, "processed", "ptb.valid.ids")
    test_path = os.path.join(data_path,  "processed", "ptb.test.ids")

    train_data = read_to_int_array(train_path)
    valid_data = read_to_int_array(valid_path)
    test_data = read_to_int_array(test_path)

    #word_to_id, id_to_word, _ = _build_vocab(_read_words_ptb, train_path)
    #train_data, _ = _file_to_word_ids(_read_words_ptb, train_path, vocabulary)
    #valid_data, _ = _file_to_word_ids(_read_words_ptb, valid_path, vocabulary)
    #test_data, _ = _file_to_word_ids(_read_words_ptb, test_path, vocabulary)
    #vocabulary = len(word_to_id)

    return train_data, valid_data, test_data, vocabulary

def umnich_iterator(raw_data, targets, batch_size, num_steps):
    """Iterate on the raw PTB data.

    This generates batch_size pointers into the raw PTB data, and allows
    minibatch iteration along these pointers.

    Args:
      raw_data: one of the raw data outputs from ptb_raw_data.
      batch_size: int, the batch size.
      num_steps: int, the number of unrolls.

    Yields:
      Pairs of the batched data, each a matrix of shape [batch_size, num_steps].
      The second element of the tuple is the same data time-shifted to the
      right by one.

    Raises:
      ValueError: if batch_size or num_steps are too high.
    """
    raw_data = np.array(raw_data, dtype=np.int32)


    data_len = len(raw_data)
    batch_len = data_len // batch_size

    data = np.zeros([batch_size, batch_len], dtype=np.int32)

    #for i in range(batch_size):
    #print(raw_data[batch_len * i:batch_len * (i + 1)])
    #    data[i] = raw_data[batch_len * i:batch_len * (i + 1)]

    #print(data.shape)

    data = np.array(np.array_split(raw_data, batch_len))

    epoch_size = (batch_len - 1) // num_steps

    if epoch_size == 0:
        raise ValueError("epoch_size == 0, decrease batch_size or num_steps")

    indieces = random.sample(range(epoch_size), epoch_size)

    for i in indieces:
        #print(i*num_steps,(i+1)*num_steps)
        x = data[i*num_steps:(i+1)*num_steps,:]
        y = targets[i*num_steps:(i+1)*num_steps]
        yield (x,y)

#def get_batch(arg):
#    pass


def ptb_iterator(raw_data, batch_size, num_steps):
    """Iterate on the raw PTB data.

    This generates batch_size pointers into the raw PTB data, and allows
    minibatch iteration along these pointers.

    Args:
      raw_data: one of the raw data outputs from ptb_raw_data.
      batch_size: int, the batch size.
      num_steps: int, the number of unrolls.

    Yields:
      Pairs of the batched data, each a matrix of shape [batch_size, num_steps].
      The second element of the tuple is the same data time-shifted to the
      right by one.

    Raises:
      ValueError: if batch_size or num_steps are too high.
    """
    raw_data = np.array(raw_data, dtype=np.int32)

    data_len = len(raw_data)
    batch_len = data_len // batch_size

    data = np.zeros([batch_size, batch_len], dtype=np.int32)

    for i in range(batch_size):
        data[i] = raw_data[batch_len * i:batch_len * (i + 1)]

    epoch_size = (batch_len - 1) // num_steps

    if epoch_size == 0:
        raise ValueError("epoch_size == 0, decrease batch_size or num_steps")

    for i in range(epoch_size):
        x = data[:, i*num_steps:(i+1)*num_steps]
        y = data[:, i*num_steps+1:(i+1)*num_steps+1]
        yield (x, y)

def create_vocabulary(vocabulary_path, data_path, max_vocabulary_size,
                      tokenizer=None, normalize_digits=True):
    """Create vocabulary file (if it does not exist yet) from data file.

    Data file is assumed to contain one sentence per line. Each sentence is
    tokenized and digits are normalized (if normalize_digits is set).
    Vocabulary contains the most-frequent tokens up to max_vocabulary_size.
    We write it to vocabulary_path in a one-token-per-line format, so that later
    token in the first line gets id=0, second line gets id=1, and so on.

    Args:
      vocabulary_path: path where the vocabulary will be created.
      data_path: data file that will be used to create vocabulary.
      max_vocabulary_size: limit on the size of the created vocabulary.
      tokenizer: a function to use to tokenize each data sentence;
        if None, basic_tokenizer will be used.
      normalize_digits: Boolean; if true, all digits are replaced by 0s.
    """
    if not gfile.Exists(vocabulary_path):
        print("Creating vocabulary %s from data %s" % (vocabulary_path, data_path))
        vocab = {}
        with gfile.GFile(data_path, mode="rb") as f:
            counter = 0
            for line in f:
                counter += 1
                if counter % 100000 == 0:
                    print("  processing line %d" % counter)
                tokens = tokenizer(line) if tokenizer else basic_tokenizer(line)
                for w in tokens:
                    word = re.sub(_DIGIT_RE, b"0", w) if normalize_digits else w
                    if word in vocab:
                        vocab[word] += 1
                    else:
                        vocab[word] = 1
            vocab_list = _START_VOCAB + sorted(vocab, key=vocab.get, reverse=True)
            if len(vocab_list) > max_vocabulary_size:
                vocab_list = vocab_list[:max_vocabulary_size]
            with gfile.GFile(vocabulary_path, mode="wb") as vocab_file:
                for w in vocab_list:
                    vocab_file.write(w + b"\n")


def initialize_vocabulary(vocabulary_path):
    """Initialize vocabulary from file.

    We assume the vocabulary is stored one-item-per-line, so a file:
      dog
      cat
    will result in a vocabulary {"dog": 0, "cat": 1}, and this function will
    also return the reversed-vocabulary ["dog", "cat"].

    Args:
      vocabulary_path: path to the file containing the vocabulary.

    Returns:
      a pair: the vocabulary (a dictionary mapping string to integers), and
      the reversed vocabulary (a list, which reverses the vocabulary mapping).

    Raises:
      ValueError: if the provided vocabulary_path does not exist.
    """
    if gfile.Exists(vocabulary_path):
        rev_vocab = []
        with gfile.GFile(vocabulary_path, mode="rb") as f:
            rev_vocab.extend(f.readlines())
        rev_vocab = [line.strip() for line in rev_vocab]
        vocab = dict([(x, y) for (y, x) in enumerate(rev_vocab)])
        return vocab, rev_vocab
    else:
        raise ValueError("Vocabulary file %s not found.", vocabulary_path)

def _padding2(lst, length):
    lst += [EOS_ID]
    if(length-len(lst) > 0):
        return lst + [PAD_ID]*(length-len(lst))

    return lst

def sentence_to_token_ids(sentence, vocabulary,
                          tokenizer=None, normalize_digits=True,
                          padding=0, eos=False):
    """Convert a string to list of integers representing token-ids.

    For example, a sentence "I have a dog" may become tokenized into
    ["I", "have", "a", "dog"] and with vocabulary {"I": 1, "have": 2,
    "a": 4, "dog": 7"} this function will return [1, 2, 4, 7].

    Args:
      sentence: the sentence in bytes format to convert to token-ids.
      vocabulary: a dictionary mapping tokens to integers.
      tokenizer: a function to use to tokenize each sentence;
        if None, basic_tokenizer will be used.
      normalize_digits: Boolean; if true, all digits are replaced by 0s.

    Returns:
      a list of integers, the token-ids for the sentence.
    """

    if tokenizer:
        words = tokenizer(sentence)
    else:
        words = basic_tokenizer(sentence)
    if not normalize_digits:
        return _padding2([vocabulary.get(w, UNK_ID) for w in words], padding)
    # Normalize digits by 0 before looking words up in the vocabulary.
    return _padding2([vocabulary.get(re.sub(_DIGIT_RE, b"0", w), UNK_ID) for w in words], padding)

def data_to_token_ids(data_path, target_path, vocabulary_path,
                      tokenizer=None, normalize_digits=True, ignoreFirst=False,
                      padding=0, eos = False):
    """Tokenize data file and turn into token-ids using given vocabulary file.

    This function loads data line-by-line from data_path, calls the above
    sentence_to_token_ids, and saves the result to target_path. See comment
    for sentence_to_token_ids on the details of token-ids format.

    Args:
      data_path: path to the data file in one-sentence-per-line format.
      target_path: path where the file with token-ids will be created.
      vocabulary_path: path to the vocabulary file.
      tokenizer: a function to use to tokenize each sentence;
        if None, basic_tokenizer will be used.
      normalize_digits: Boolean; if true, all digits are replaced by 0s.
    """
    if not gfile.Exists(target_path):
        print("Tokenizing data in %s" % data_path)
        vocab, _ = initialize_vocabulary(vocabulary_path)
        with gfile.GFile(data_path, mode="rb") as data_file:
            with gfile.GFile(target_path, mode="w") as tokens_file:
                counter = 0
                for line in data_file:
                    counter += 1
                    if counter % 100000 == 0:
                        print("  tokenizing line %d" % counter)
                    token_ids = sentence_to_token_ids(line, vocab, tokenizer,
                                                      normalize_digits, padding=padding)

                    tokens_file.write(" ".join([str(tok) for tok in token_ids]) + "\n")


def prepare_data(data_dir, vocabulary_size, tokenizer=None):
    """Get WMT data into data_dir, create vocabularies and tokenize data.

    Args:
      data_dir: directory in which the data sets will be stored.
      en_vocabulary_size: size of the English vocabulary to create and use.
      fr_vocabulary_size: size of the French vocabulary to create and use.
      tokenizer: a function to use to tokenize each data sentence;
        if None, basic_tokenizer will be used.

    Returns:
      A tuple of 6 elements:
        (1) path to the token-ids for English training data-set,
        (2) path to the token-ids for French training data-set,
        (3) path to the token-ids for English development data-set,
        (4) path to the token-ids for French development data-set,
        (5) path to the English vocabulary file,
        (6) path to the French vocabulary file.
    """

    vocab_path = os.path.join(data_dir, "processed", "vocab.txt")
    data_path  = os.path.join(data_dir, "merged.txt")

    # Create vocabulary.
    create_vocabulary(vocab_path, data_path, vocabulary_size, tokenizer, False)

    # Create token ids for the training data.
    ptb_train_path      = os.path.join(data_dir, "ptb.train.txt")
    ptb_train_ids_path  = os.path.join(data_dir, "processed", "ptb.train.ids")

    # Create token ids for the training data.
    ptb_test_path      = os.path.join(data_dir, "ptb.test.txt")
    ptb_test_ids_path  = os.path.join(data_dir, "processed", "ptb.test.ids")

    # Create token ids for the training data.
    ptb_valid_path      = os.path.join(data_dir, "ptb.valid.txt")
    ptb_valid_ids_path  = os.path.join(data_dir, "processed", "ptb.valid.ids")

    umich_data_path = os.path.join(data_dir, "umich.train.sentence.txt")
    umich_ids_path  = os.path.join(data_dir, "processed", "umich.train.ids")

    data_to_token_ids(ptb_train_path, ptb_train_ids_path, vocab_path, tokenizer)
    data_to_token_ids(ptb_test_path, ptb_test_ids_path, vocab_path, tokenizer)
    data_to_token_ids(ptb_valid_path, ptb_valid_ids_path, vocab_path, tokenizer)
    data_to_token_ids(umich_data_path, umich_ids_path, vocab_path, tokenizer, padding=49)

    return umich_ids_path, umich_ids_path, vocab_path
