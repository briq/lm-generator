import numpy as np
from util import reader as reader
import tensorflow as tf

def sample(a, temperature=1.0):
    # helper function to sample an index from a probability array
    a = np.log(a) / temperature
    a = np.exp(a) / np.sum(np.exp(a))
    return np.argmax(np.random.multinomial(1, a, 1))

def generate_text(session,m,eval_op):
    state = m.initial_state.eval()

    x = np.zeros((m.batch_size,m.num_steps), dtype=np.int32)

    output = str()
    for i in xrange(m.batch_size):
        for step in xrange(m.num_steps):
            try:
                feed_dict = {m.input_data: x, m.targets: x, m.initial_state: state}
                cost, state, _, probabilities = session.run(
                    [m.cost, m.final_state, eval_op, m.probabilities],
                    feed_dict=feed_dict
                )
                word_id     = sample(probabilities[0,:]) # why zero//
                output      = output + " " + reader.word_from_id(word_id)
                x[i][step]  = word_id

            except ValueError as e:
                print("ValueError")

    print(output)
