# Copyright 2015 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""
Prototype implementation for a masters thesis project, generating conceptually
transformed sequences of text.

The network functions as a sequence-to-sequence autoencoder

The network consists of a encoder, decoder and a sentiment classifier.

The encoder inputs are the sentences that are to be encoded.
The decoder inputs are the same as the encoder inputs

The hyperparameters used in the model:
- init_scale - the initial scale of the weights
- learning_rate - the initial value of the learning rate
- max_grad_norm - the maximum permissible norm of the gradient
- num_layers - the number of LSTM layers
- num_steps - the number of unrolled steps of LSTM
- hidden_size - the number of LSTM units
- max_epoch - the number of epochs trained with the initial learning rate
- max_max_epoch - the total number of epochs for training
- keep_prob - the probability of keeping weights in the dropout layer
- lr_decay - the decay of the learning rate for each epoch after "max_epoch"
- batch_size - the batch size
- checkpoint_dir - the directory containing model checkpoints

The data required for this example is in the data/ dir of the

To run:
$ python train.py --data_path=simple-examples/data/

"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os, sys

import time

import tensorflow.python.platform

import numpy as np

# from sklearn.manifold import TSNE
# import matplotlib
# matplotlib.use('Agg')
# import matplotlib.pyplot as plt

import tensorflow as tf

from tensorflow.python.ops import rnn, rnn_cell, seq2seq

from util import reader as reader

from tensorflow.python.platform import gfile

import models.transform
import generator
import config

flags = tf.flags
logging = tf.logging

flags.DEFINE_string("model", "small", "A type of model. Possible options are: small, medium, large.")
flags.DEFINE_string("checkpoint_dir", "data/checkpoints/", "Location where the state is saved")
flags.DEFINE_string("data_path", None, "data_path")
flags.DEFINE_integer("ptb_vocab_size", 11310, "PTB vocabulary size.")
flags.DEFINE_boolean("output_tsne", False, "When set to true, generate t-SNE of the word embeddings")

FLAGS = flags.FLAGS

VOCABULARY = {}
REVERSED_VOCABULARY = {}

def get_config():
  if FLAGS.model == "small":
    return config.SmallConfig()
  elif FLAGS.model == "medium":
    return config.MediumConfig()
  elif FLAGS.model == "large":
    return config.LargeConfig()
  else:
    raise ValueError("Invalid model: %s", FLAGS.model)

def createModels(session, initializer):
    gen_config = config.SmallGenConfig()

    conf = get_config()

    eval_config = get_config()
    eval_config.batch_size  = 1
    eval_config.num_steps   = 1

    with tf.variable_scope("model", reuse=None, initializer=initializer):
        model_train = models.transform.TransformModel("train", False,
            conf.learning_rate, conf.lr_decay,
            config=conf)

    with tf.variable_scope("model", reuse=True, initializer=initializer):
        model_valid = models.transform.TransformModel("valid", True,
            conf.learning_rate, conf.lr_decay,
            config=conf)

        model_test  = models.transform.TransformModel("test", True,
            conf.learning_rate, conf.lr_decay,
            config=eval_config)

        model_gen = models.transform.TransformModel("gen", True,
            gen_config.learning_rate, gen_config.lr_decay,
            config=gen_config)



    return model_train, model_valid, model_test, model_gen

def train(vocabulary, vocabularyLookup):
    config = get_config()

    # Get data from PTB dataset for the LM
    encoder_train_data, encoder_valid_data, encoder_test_data, _ = (
        reader.ptb_raw_data(vocabulary, FLAGS.data_path))

    # Get data from UMNICH dataset for classifier
    cn_train_data, cn_test_data, cn_vocabulary, cn_train_targets, cn_test_targets = (
        reader.umich_raw_data(config.batch_size, vocabulary, FLAGS.data_path))

    # Create graph and run training
    with tf.Graph().as_default(), tf.Session() as session:
        writer = tf.train.SummaryWriter("/tmp/tb_logs", session.graph_def)

        initializer = tf.random_uniform_initializer(-config.init_scale,
                                                   config.init_scale)


        # Create models
        model_train, model_valid, model_test, model_gen = createModels(
            session, initializer)

        # Try to restore the model
        path = getCheckpointPath()
        saveHyperParameters(config)
        ckpt = tf.train.get_checkpoint_state(path)

        if ckpt and gfile.Exists(ckpt.model_checkpoint_path):
            print("Reading model parameters from {0}".format(ckpt.model_checkpoint_path))
            model_train.saver.restore(session, ckpt.model_checkpoint_path)
        else:
            print("Created model with fresh parameters.")
            session.run(tf.initialize_all_variables())

        #print("Generating")
        #generate(session, model_gen, vocabularyLookup)

        #print("Training the encoder")
        #train_encoder(session, model_train, model_valid, model_test, config,
        #     encoder_train_data, encoder_valid_data, encoder_test_data, writer)

        #print("Training the classifier")
        #train_sentiment(session, model_train, config, cn_train_data, cn_test_data,
        #    cn_vocabulary, cn_train_targets, cn_test_targets, writer)

        print("Training the decoder")
        train_decoder(session, model_train, model_valid, model_test, config,
             encoder_train_data, encoder_valid_data, encoder_test_data, writer)

def save_model(sess, model):
    path = getCheckpointPath()

    checkpoint_path = os.path.join(path, "transform"+model.name+".ckpt")
    model.saver.save(sess, checkpoint_path, global_step=model.global_step)

def train_sentiment(session, model, config, train_data, test_data,
    vocabulary, train_targets, test_targets, writer):
    """ Training procedure for the sentiment classifier

    The sentiment classifier consists of a language model (PTBModel),
    connected to a mean pooling layer, connected to a logistic regression layer.
    """

    # Run training max_max_epoch times
    for i in range(config.max_max_epoch):
        # lr_decay = lr_decay ^ {i - config.max_epoch}
        lr_decay = config.lr_decay ** max(i - config.max_epoch, 0.0)

        # temperature (learning rate) decreasing over time
        model.assign_classifier_lr(session, config.learning_rate * lr_decay)
        model.assign_encoder_lr(session, 0.01 * lr_decay)

        session.run(model.encoder_lr)
        session.run(model.classifier_lr)

        # Run one epoch of the training
        print("Running epoch:", i + 1)
        model.sentiment_epoch(session, i, train_data, train_targets,
            writer, verbose=True, is_training=True)
        model.sentiment_epoch(session, i, test_data, test_targets,
            writer, verbose=True, is_training=False)

        if FLAGS.output_tsne:
            plot_embeddings(model_train, "classifier"+str(i)+".png")

        # Save model
        save_model(session, model)

def train_decoder(session, model_train, model_valid, model_test, config,
    train_data, valid_data, test_data, writer):
    """ Training procedure for the Language Model.

    Returns: A trained language model (PTBModel)
    """

    if not FLAGS.data_path:
        raise ValueError("Must set --data_path to PTB data directory")

    model_train.assign_decoder_lr(session, config.learning_rate)

    for i in range(config.max_max_epoch):
        # lr_decay = lr_decay ^ {i - config.max_epoch}
        # lr_decay = config.lr_decay ** max(i - config.max_epoch, 0.0)

        # temperature (learning rate) decreasing over time

        print("Epoch: %d Learning rate: %.3f" % (i + 1, session.run(model_train.decoder_lr)))
        train_perplexity, _ = model_train.decoder_epoch(session, i, train_data, writer,
           verbose=True)

        # if FLAGS.output_tsne:
        #     plot_embeddings(model_train, "decoder"+str(i)+".png")

        print("Epoch: %d Train Perplexity: %.3f" % (i + 1, train_perplexity))

        valid_perplexity, _ = model_valid.decoder_epoch(session, i, valid_data, writer)
        print("Epoch: %d Valid Perplexity: %.3f" % (i + 1, valid_perplexity))

        # Save model
        save_model(session, model_train)

    #test_perplexity, _ = model_test.decoder_epoch(session, i, test_data, writer)
    #print("Test Perplexity: %.3f" % test_perplexity)

def train_encoder(session, model_train, model_valid, model_test, config,
    train_data, valid_data, test_data, writer):
    """ Training procedure for the Language Model.

    Returns: A trained language model (PTBModel)
    """

    if not FLAGS.data_path:
        raise ValueError("Must set --data_path to PTB data directory")

    model_train.assign_encoder_lr(session, config.learning_rate)

    for i in range(config.max_max_epoch):
        # lr_decay = lr_decay ^ {i - config.max_epoch}
        #lr_decay = config.lr_decay ** max(i - config.max_epoch, 0.0)

        # temperature (learning rate) decreasing over time

        print("Epoch: %d Learning rate: %.3f" % (i + 1, session.run(model_train.encoder_lr)))
        train_perplexity, _ = model_train.encoder_epoch(session, i, train_data, writer,
                               verbose=True)

        # if FLAGS.output_tsne:
        #     plot_embeddings(model_train, "encoder"+str(i)+".png")

        print("Epoch: %d Train Perplexity: %.3f" % (i + 1, train_perplexity))

        valid_perplexity, _ = model_valid.encoder_epoch(session, i, valid_data, writer)
        print("Epoch: %d Valid Perplexity: %.3f" % (i + 1, valid_perplexity))

        # Save model
        save_model(session, model_train)

    # test_perplexity, _ = model_test.encoder_epoch(session, i, test_data, writer)
    # print("Test Perplexity: %.3f" % test_perplexity)

def generate(session, model_gen, vocabulary):
    # Restore variables from disk.
    getCheckpointPath()
    #saver = tf.train.Saver()
    #saver.restore(session, model_path)
    #print("Model restored from file " + model_path)

    model_gen.generate_text(session, vocabulary, 10)

def main(unused_args):
    config = get_config()

    print("Preparing data in %s" % FLAGS.data_path)
    # TODO: Should we use the tokenizer?
    # TODO: Dont tokenize ids
    _, _, vocab_path = reader.prepare_data(FLAGS.data_path, config.vocab_size)

    global VOCABULARY, REVERSED_VOCABULARY
    VOCABULARY, REVERSED_VOCABULARY = reader.initialize_vocabulary(vocab_path)

    train(VOCABULARY, REVERSED_VOCABULARY)

    # inputs  = np.load("input.npy")
    # outputs = np.load("output.npy")
    #
    # for inp in inputs:
    #     print(" ".join([REVERSED_VOCABULARY[w]  for w in inp]))
    #
    # print("========")
    #
    # for inp in outputs:
    #     print(" ".join([REVERSED_VOCABULARY[w]  for w in inp]))


def getCheckpointPath():
    '''
    Check if new hyper params match with old ones
    if not, then create a new model in a new Directory
    Returns:
    path to checkpoint directory
    '''
    config = get_config()

    old_path = os.path.join(FLAGS.checkpoint_dir, "hyperparams.npy")
    if os.path.exists(old_path):
        params = np.load(old_path)
        ok = \
        params[3] == config.num_layers and \
        params[1] == config.hidden_size and \
        params[2] == config.keep_prob and \
        params[0] == config.vocab_size
        if ok:
            return FLAGS.checkpoint_dir
        else:
            infostring = "hiddensize_{0}_dropout_{1}_numlayers_{2}".format(config.hidden_size,
            config.keep_prob, config.num_layers)
            path = os.path.join("data/checkpoints/", str(int(time.time())) + infostring)
            if not os.path.exists(path):
                os.makedirs(path)
            print("hyper parameters changed, training new model at {0}".format(path))
            return path
    else:
        return FLAGS.checkpoint_dir

# def plot_embeddings(model, filename):
#     try:
#         final_embeddings = model.normalized_embeddings.eval()
#         tsne = TSNE(perplexity=30, n_components=2, init='pca', n_iter=5000)
#         plot_only = 500
#         low_dim_embs = tsne.fit_transform(final_embeddings[:plot_only,:])
#         labels = [REVERSED_VOCABULARY[i] for i in xrange(plot_only)]
        plot_with_labels(low_dim_embs, labels, filename=filename)
#     except:
#         print("Error")

# def plot_with_labels(low_dim_embs, labels, filename='tsne.png'):
#     assert low_dim_embs.shape[0] >= len(labels), "More labels than embeddings"
#     plt.figure(figsize=(18, 18))  #in inches
#     for i, label in enumerate(labels):
#         x, y = low_dim_embs[i,:]
#         plt.scatter(x, y)
#         plt.annotate(label,
#                      xy=(x, y),
#                      xytext=(5, 2),
#                      textcoords='offset points',
#                      ha='right',
#                      va='bottom')
#
#     plt.savefig(filename)

def saveHyperParameters(config):
    hParams = np.array([config.vocab_size, config.hidden_size,
    config.keep_prob, config.num_layers, config.max_grad_norm,
    config.learning_rate, config.lr_decay])
    path = os.path.join(FLAGS.checkpoint_dir, "hyperparams.npy")
    np.save(path, hParams)

if __name__ == "__main__":
  tf.app.run()
