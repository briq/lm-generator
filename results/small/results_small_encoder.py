import matplotlib.pyplot as plt

valid_data = [13018.097, 12664.592,8812.253,731.709,550.156,430.747,364.414,
  297.316,279.576,240.066,212.122,206.045,204.920,190.625,189.385,190.383,190.308,
  191.040,188.544,191.188,190.560,193.154,195.248,196.653,198.895]

train_data = [12997.041, 12882.632, 9989.988,3741.650,792.357,588.749,447.342,
  341.501,266.196,215.618,183.666,161.073,143.804,130.142,119.252,110.213,102.155,
  95.627,90.113,84.787,80.645,76.738,73.358,70.124,67.282]


plt.ylabel('Perplexity')
plt.xlabel('Epochs')

plt.scatter(range(len(valid_data)),valid_data, c='r')
plt.scatter(range(len(train_data)),train_data, c='b')

plt_valid, = plt.plot(range(len(valid_data)), valid_data, c='r')
plt_train, = plt.plot(range(len(train_data)), train_data, c='b')

plt.legend([plt_valid, plt_train], ['Validation','Training'])

plt.show()
