import time

import tensorflow.python.platform

import numpy as np
import tensorflow as tf

from tensorflow.python.ops import rnn, rnn_cell, seq2seq

from util import reader as reader
from util import math

# Based on https://github.com/inikdom/neural-sentiment
class TransformModel(object):
    """The generator"""
    def __init__(self, name, forward_only, learning_rate, lr_decay, config,
        tsne=False, phase=3):

        # Count of the number of training steps
        self.global_step = tf.Variable(0, trainable=False)

        # Declare if the models should update or not
        self.forward_only       = forward_only

        # Get configuration variables
        self.name               = name
        self.num_classes        = config.num_classes
        self.batch_size         = batch_size = config.batch_size
        self.num_steps          = num_steps = config.num_steps
        self.projection_dim     = config.hidden_size
        self.max_gradient_norm  = config.max_grad_norm
        self.dropout            = config.keep_prob
        self.max_seq_length     = self.num_steps
        self.hidden_size        = config.hidden_size
        self.vocab_size         = config.vocab_size

        # Set the beam size for the generator
        self.beam_size          = config.beam_size

        # Placeholders for the input data and targets
        # The output from the language model has the dimension
        #   1600*vocab_size
        self._input_data        = tf.placeholder(tf.int32, shape=[None, num_steps], name="input")
        self._encoder_inputs    = self._input_data
        self._encoder_targets   = tf.placeholder(tf.int32, shape=[self.batch_size, self.num_steps], name="target")
        self.classifier_target  = tf.placeholder(tf.float32, shape=[self.batch_size,self.num_classes], name="classifier_target")
        self._decoder_inputs    = tf.placeholder(tf.int32, shape=[None, num_steps], name="decoder_input")

        self.dropout_keep_prob_embedding      = tf.constant(self.dropout)
        self.dropout_keep_prob_lstm_input     = tf.constant(self.dropout)
        self.dropout_keep_prob_lstm_output    = tf.constant(self.dropout)

        # Forces the operations to be executed on the CPU
        with tf.variable_scope("embedding"), tf.device("/cpu:0"):
            W = tf.get_variable(
				"W",
				[self.vocab_size, self.hidden_size],
				initializer=tf.random_uniform_initializer(-1.0, 1.0))

            # encoder tokens
            self.embedded_tokens = tf.nn.embedding_lookup(W, self._input_data)
            self.embedded_tokens_drop = tf.nn.dropout(
                self.embedded_tokens, self.dropout_keep_prob_embedding)

            # decoder tokens
            if not forward_only:
                self.decoder_embedded_tokens_drop = self.embedded_tokens_drop
            else:
                self.decoder_embedded_tokens = tf.nn.embedding_lookup(
                    W, self._decoder_inputs)
                self.decoder_embedded_tokens_drop = tf.nn.dropout(
                    self.decoder_embedded_tokens,
                    self.dropout_keep_prob_embedding)

            # Optimization, only needed when visualizing
            if tsne:
                # t-SNE vizualizations
                # Compute the cosine similarity between minibatch examples and all embeddings.
                valid_size = 16     # Random set of words to evaluate similarity on.
                valid_window = 100  # Only pick dev samples in the head of the distribution.
                valid_examples = np.random.choice(valid_window, valid_size, replace=False)

                valid_dataset = tf.constant(valid_examples, dtype=tf.int32)

                embedding = tf.get_variable("embedding", [self.vocab_size, self.hidden_size])

                norm = tf.sqrt(tf.reduce_sum(tf.square(embedding), 1, keep_dims=True))
                self.normalized_embeddings = embedding / norm
                valid_embeddings = tf.nn.embedding_lookup(
                    self.normalized_embeddings, valid_dataset)
                self.similarity = tf.matmul(
                    valid_embeddings, self.normalized_embeddings, transpose_b=True)

        # Mean pooling of the language models outputs
        # TODO: Ensure that this is correctly using the language model
        # NOTE: If we are using the RNN scope the softmax and embedding
        # variables are not trained, is this nesesary?
        with tf.variable_scope("lstm") as scope:
            # Create lstm cell
            lstm_cell = rnn_cell.DropoutWrapper(
                rnn_cell.LSTMCell(self.hidden_size, state_is_tuple=False, initializer=tf.random_uniform_initializer(-1.0, 1.0)),
                input_keep_prob=self.dropout_keep_prob_lstm_input,
                output_keep_prob=self.dropout_keep_prob_lstm_output)
            self._cell = cell = rnn_cell.MultiRNNCell([lstm_cell] * config.num_layers, state_is_tuple=False)

            # Build the recurrence. We do this manually to use truncated backprop
            self._initial_state   = tf.zeros([self.batch_size, self.cell.state_size])
            self.encoder_states  = [self.initial_state]
            self.encoder_outputs = []
            for time_step in range(self.num_steps):
                if time_step > 0:
                	scope.reuse_variables()
                new_output, new_state = self.cell(self.embedded_tokens_drop[:, time_step, :], self.encoder_states[-1])

                self.encoder_outputs.append(new_output)
                self.encoder_states.append(new_state)

            concat_states       = tf.pack(self.encoder_states)
            avg_states          = tf.reduce_mean(concat_states, 0)

            self._encoder_final_state = self.encoder_states[-1]

            # split the ccncatenated state into cell state and hidden state
            _, self._classifier_final_state = tf.split(1,2,avg_states)

            # Outputs for the minibatch
            output = tf.reshape(tf.concat(1, self.encoder_outputs), [-1, self.hidden_size])
            self._output = output

        # Weights connecting the LSTMs to the softmax
        softmax_w = tf.get_variable("softmax_w", [self.hidden_size, self.vocab_size])
        softmax_b = tf.get_variable("softmax_b", [self.vocab_size])

        self._encoder_logits    = tf.matmul(self.output, softmax_w) + softmax_b
        self._probabilities     = tf.nn.softmax(self.encoder_logits)

        # Compute the cross-entropy loss betweeen LM logits and target
        # sequence_loss_by_example(logits, targets, weights, num_decoder_symbols (output classes))
        # Logits are the output from the LSTMs
        # Targets are simply the input data shifted by one time-step
        # Weights are all unity since the LSTMs will have unity connections to the previous states
        # The number of classes is the number of words in the vocabulary
        encoder_loss = seq2seq.sequence_loss_by_example([self.encoder_logits],
                                                [tf.reshape(self._encoder_targets, [-1])],
                                                [tf.ones([batch_size * num_steps])],
                                                self.vocab_size)

        self._cost = cost = tf.reduce_sum(encoder_loss) / batch_size
        self.encoder_loss_summ = tf.scalar_summary(
            "{0}_encoder_loss".format(self.name), self.cost)

        self._encoder_lr        = tf.Variable(0.0, trainable =False)
        self._classifier_lr     = tf.Variable(0.0, trainable =False)

        # Get trainable variables and comput, apply gradients
        tvars          = tf.trainable_variables()
        grads, _       = tf.clip_by_global_norm(tf.gradients(cost, tvars),
                                          config.max_grad_norm)
        optimizer      = tf.train.GradientDescentOptimizer(self.encoder_lr)

        self._encoder_train_op = optimizer.apply_gradients(
            zip(grads, tvars), global_step=self.global_step)

        ######################################################################
        # Code for the classfier
        ######################################################################

        # Predict output labels
        with tf.variable_scope("output_projection"):
            W = tf.get_variable(
            	"W",
            	[self.hidden_size*config.num_layers, self.num_classes],
            	initializer=tf.truncated_normal_initializer(stddev=0.1))
            b = tf.get_variable(
            	"b",
            	[self.num_classes],
            	initializer=tf.constant_initializer(0.1))

            self.scores      = tf.nn.xw_plus_b(self._classifier_final_state, W, b)
            self.y           = tf.nn.softmax(self.scores)
            self.predictions = tf.argmax(self.scores, 1)

        # Compare output labels to targets
        #   Measured using cross-entropy error
        with tf.variable_scope("loss"):
            self.losses     = tf.nn.softmax_cross_entropy_with_logits(self.scores, self.classifier_target, name ="losses")
            self.total_loss = tf.reduce_sum(self.losses)
            self.mean_loss  = tf.reduce_mean(self.losses)

        with tf.variable_scope("accuracy"):
            self.correct_predictions = tf.equal(self.predictions, tf.argmax(self.classifier_target, 1))
            self.accuracy            = tf.reduce_mean(tf.cast(self.correct_predictions, "float"), name="accuracy")

        # Get trainable variables for the classifier
        with tf.variable_scope("output_projection") as vs:
            classifier_parameters = [v for v in tf.trainable_variables()
                    if v.name.startswith(vs.name)]

        # Get all trainable variables
        params = set(tf.trainable_variables())
        encoder_parameters = params - set(classifier_parameters)
        encoder_parameters = list(encoder_parameters)

        with tf.name_scope("train") as scope:
            opt  = tf.train.GradientDescentOptimizer(self.classifier_lr)
            opt_encoder = tf.train.GradientDescentOptimizer(self.encoder_lr)

        gradients = tf.gradients(self.losses, encoder_parameters + classifier_parameters)
        clipped_gradients, norm = tf.clip_by_global_norm(gradients, self.max_gradient_norm)

        with tf.name_scope("grad_norms") as scope:
             grad_summ = tf.scalar_summary("grad_norms".format(self.name), norm)

        grads1 = clipped_gradients[:len(encoder_parameters)]
        grads2 = clipped_gradients[len(encoder_parameters):]

        train_op1 = opt_encoder.apply_gradients(zip(grads1, encoder_parameters))
        train_op2 = opt.apply_gradients(zip(grads2, classifier_parameters))

        self.update = tf.group(train_op1, train_op2)

        encoder_lr_summary = tf.scalar_summary("{0}_encoder_lr".format(self.name), self.encoder_lr)
        cn_lr_summary = tf.scalar_summary("{0}_cn_lr".format(self.name), self.classifier_lr)

        loss_summ = tf.scalar_summary("{0}_cn_loss".format(self.name), self.mean_loss)
        acc_summ = tf.scalar_summary("{0}_cn_accuracy".format(self.name), self.accuracy)
        self.merged = tf.merge_summary([loss_summ, acc_summ, grad_summ, encoder_lr_summary, cn_lr_summary])

        # Optimization, if we are training only the language model we will not need
        # the rest of the code
        if phase < 3:
            return

        ######################################################################
        # Code for decoder
        ######################################################################

        with tf.variable_scope("decoder") as scope:
            # Create lstm cell
            lstm_cell = rnn_cell.DropoutWrapper(
                rnn_cell.LSTMCell(self.hidden_size, initializer=tf.random_uniform_initializer(-1.0, 1.0)),
                input_keep_prob=self.dropout_keep_prob_lstm_input,
                output_keep_prob=self.dropout_keep_prob_lstm_output)
            self._decoder_cell = decoder_cell = rnn_cell.MultiRNNCell([lstm_cell] * config.num_layers)

            # TODO: Should the backprop in the decoder affect the encoder
            # The autoencoder is initialized to the final state of the language model
            # avg_states is the last state of Language model 1
            self._decoder_inital_state = self.encoder_final_state
            decoder_state = self.decoder_inital_state

            decoder_outputs = []
            if not self.forward_only:
                for time_step in range(num_steps):
                    if time_step > 0: tf.get_variable_scope().reuse_variables()
                    (cell_output, decoder_state) = decoder_cell(
                        self.decoder_embedded_tokens_drop[:, time_step, :],
                        decoder_state)
                    decoder_outputs.append(cell_output)
            else:
                cell_output = self.encoder_outputs[-1]
                for time_step in range(num_steps):
                    if time_step > 0: tf.get_variable_scope().reuse_variables()
                    (cell_output, decoder_state) = decoder_cell(cell_output, decoder_state)
                    decoder_outputs.append(cell_output)

            decoder_output = tf.reshape(tf.concat(1, decoder_outputs), [-1, self.hidden_size])
            self._decoder_output = decoder_output

        # Weights connecting the LSTMs to the softmax
        softmax_w = tf.get_variable("decoder_softmax_w", [self.hidden_size, self.vocab_size])
        softmax_b = tf.get_variable("decoder_softmax_b", [self.vocab_size])

        self._decoder_logits        = tf.matmul(self.decoder_output, softmax_w) + softmax_b
        self._decoder_probabilities = tf.nn.softmax(self.decoder_logits)

        self._output_probs = tf.nn.softmax(self.encoder_logits)

        # Compute the cross-entropy loss betweeen LM logits and target
        # sequence_loss_by_example(logits, targets, weights, num_decoder_symbols (output classes))
        # Logits are the output from the LSTMs
        # Targets are simply the input data shifted by one time-step
        # Weights are all unity since the LSTMs will have unity connections to the previous states
        # The number of classes is the number of words in the vocabulary
        # TODO: Do we need to prepend the "go" symbol
        decoder_loss = seq2seq.sequence_loss_by_example([self.decoder_logits],
                                                [tf.reshape(self._encoder_targets, [-1])],
                                                [tf.ones([batch_size * num_steps])],
                                                self.vocab_size)

        self._decoder_cost = tf.reduce_sum(decoder_loss) / batch_size

        self.decoder_loss_summ = tf.scalar_summary(
            "{0}_decoder_loss".format(self.name), self.decoder_cost)

        self._decoder_final_state = decoder_state

        self._decoder_lr = tf.Variable(0.0, trainable =False)

        with tf.variable_scope("decoder") as scope:
            # Get trainable variables and comput, apply gradients
            decoder_vars = [v for v in tf.trainable_variables()
                        if v.name.startswith(scope.name)]

        grads, _  = tf.clip_by_global_norm(tf.gradients(self.decoder_cost,
                    decoder_vars), config.max_grad_norm)
        optimizer = tf.train.GradientDescentOptimizer(self.decoder_lr)

        # backpropagate decoder error to encoder, embedding
        autoencoder_vars = decoder_vars
        autoencoder_vars.extend(encoder_parameters)

        self._decoder_train_op = optimizer.apply_gradients(
            zip(grads, autoencoder_vars), global_step=self.global_step)

        # Define a saver object, to save all trained variables
        self._saver = tf.train.Saver(tf.all_variables())

    def autoencoder_epoch(self, session, epoch, data, writer, verbose=False):
        # // -> integer division
        epoch_size = ((len(data) // self.batch_size) - 1) // self.num_steps
        start_time = time.time()
        costs = 0.0
        iters = 0
        state = self.initial_state.eval()
        logits = []
        inputs = []

        for step, (x, y) in enumerate(reader.ptb_iterator(data, self.batch_size,
                                                          self.num_steps)):
            costs, iters, state, logit = self.encoder_step(session, data, writer,
                (x,y), step, epoch_size, start_time, costs, iters, state, verbose=False)

            costs, iters, state, logit = self.decoder_step(session, data, writer,
                (x,y), step, epoch_size, start_time, costs, iters, state, verbose=False)

            logits.append(logit)
            inputs.append(x)

        return np.exp(costs / iters), logits, inputs

    def generator_epoch(self, session, epoch, data, writer, verbose=False):
        # // -> integer division
        epoch_size = ((len(data) // self.batch_size) - 1) // self.num_steps
        start_time = time.time()
        costs = 0.0
        iters = 0
        state = self.initial_state.eval()
        logits = []

        for step, (x, y) in enumerate(reader.ptb_iterator(data, self.batch_size,
                                                          self.num_steps)):
            #costs, iters, state, logit = self.encoder_step(session, data, writer,
            #    (x,y), step, epoch_size, start_time, costs, iters, state, verbose=False)


            output = self.generate_step_alt(session, x)
            break

        return x, output

    def generate_text(self, session, words, num_sentences):

        state = self.initial_state.eval()
        x = 2 # the id for '<eos>' from the training set
        inp = np.matrix([[x]])  # a 2D numpy matrix

        text = b""
        count = 0

        while count < num_sentences:
          output_probs, state = session.run([self.output_probs, self.encoder_final_state],
                                       {self.input_data: inp,
                                        self.initial_state: state})

          x = math.sample(output_probs[0], 0.9)
          if words[x] == b"<EOS>":
            text += b".\n\n"
            count += 1
          else:
            text = text + b" " + words[x]
          # now feed this new word as input into the next iteration
          inp = np.matrix([[x]])

        print(text)

        return

    def get_vocab(filename):
      data = _read_words(filename)

      counter = collections.Counter(data)
      count_pairs = sorted(counter.items(), key=lambda x: (-x[1], x[0]))

      words, _ = list(zip(*count_pairs))

      return words

    def generate_step_alt(self, session, inputs):

        # start with zero matrix as input
        x = np.zeros((self.batch_size,self.num_steps), dtype=np.int32)

        # dummy targets
        y = inputs


        # for i in xrange(self.batch_size):
        #     for step in xrange(self.num_steps):
        #
        #         feed_dict = {self.input_data: inputs,self.decoder_inputs: x, self.encoder_targets: y}
        #
        #         cost, state, output, logits = session.run(
        #             [self.decoder_cost, self.decoder_final_state,
        #             self.decoder_output, self.decoder_logits],
        #             feed_dict=feed_dict
        #         )
        #
        #         sentences = np.split(np.array(logits), self.batch_size)
        #
        #         x[i,step] = int(np.argmax(logits[(i+1)*step,:], axis=0))

        encoder_state = self.initial_state.eval()

        feed_dict = {self.initial_state: encoder_state,
            self.input_data: inputs, self.decoder_inputs: x,
            self.encoder_targets: y}

        encoder_state, decoder_state, cost, output, logits, probabilities = session.run(
            [self.encoder_final_state, self.decoder_final_state,
            self.decoder_cost, self.decoder_output, self.decoder_logits,
            self.decoder_probabilities],
            feed_dict=feed_dict
        )

        for step in xrange(self.num_steps):

            sentences = np.vsplit(np.array(probabilities), self.batch_size)

            for i, sentence in enumerate(sentences):
                x[i,step] = int(np.argmax(sentence[step,:], axis=0))
                #print(x)
                # try:
                #     x[i,step] = self.sample(sentence[step,:])
                # except:
                #     x[i,step] = 0
        return x

    def generate_step(self, session, inputs):

        # start with zero matrix as input
        x = np.zeros((self.batch_size,self.num_steps), dtype=np.int32)

        # dummy targets
        y = inputs


        # for i in xrange(self.batch_size):
        #     for step in xrange(self.num_steps):
        #
        #         feed_dict = {self.input_data: inputs,self.decoder_inputs: x, self.encoder_targets: y}
        #
        #         cost, state, output, logits = session.run(
        #             [self.decoder_cost, self.decoder_final_state,
        #             self.decoder_output, self.decoder_logits],
        #             feed_dict=feed_dict
        #         )
        #
        #         sentences = np.split(np.array(logits), self.batch_size)
        #
        #         x[i,step] = int(np.argmax(logits[(i+1)*step,:], axis=0))


        for step in xrange(self.num_steps):

            encoder_state = self.initial_state.eval()

            feed_dict = {self.initial_state: encoder_state,
                self.input_data: inputs, self.decoder_inputs: x,
                self.encoder_targets: y}

            encoder_state, decoder_state, cost, output, logits, probabilities = session.run(
                [self.encoder_final_state, self.decoder_final_state,
                self.decoder_cost, self.decoder_output, self.decoder_logits,
                self.decoder_probabilities],
                feed_dict=feed_dict
            )

            print(cost)

            sentences = np.vsplit(np.array(probabilities), self.batch_size)

            # for i, sentence in enumerate(sentences):
            #     # x[i,step] = int(np.argmax(sentence[step,:], axis=0))
            #     # #print(x)
            #     try:
            #         x[i,step] = self.sample(sentence[step,:])
            #     except:
            #         x[i,step] = 0
        return x




    def sentiment_step(self, session, epoch, xy, step, epoch_size,
        writer, is_training, verbose=False):

            (x,y) = xy
            # Inputs to the sentiment model
            input_feed = {self.input_data: x, self.classifier_target: y}

            # Run the model on the batch
            # if not training, don't backproagate error
            if is_training:
                output_feed = [self.merged, self.mean_loss, self.accuracy,
                    self.update]

                merged, mean_loss, accuracy, update = session.run(
                    output_feed, feed_dict=input_feed)
            else:
                output_feed = [self.merged, self.mean_loss, self.y,
                    self.accuracy]

                merged, mean_loss, y, accuracy = session.run(
                    output_feed, feed_dict=input_feed)

            return mean_loss, accuracy, merged

    def sentiment_epoch(self, session, epoch, data, targets, writer,
        is_training=False, verbose=False):
        epoch_size = ((len(data) // self.batch_size) - 1) // self.num_steps
        #state = self.initial_state.eval()
        iters       = 0
        loss        = 0.0
        accuracy    = 0.0

        for step, (x, y) in enumerate(reader.umnich_iterator(data, targets, self.batch_size,
                                                        self.num_steps)):

            y = np.array(y)

            mean_loss, step_accuracy, str_summary = self.sentiment_step(
                session, epoch, (x,y), step, epoch_size,
                writer, is_training, verbose)

            loss  = loss + mean_loss

            iters = iters + 1

            if self.forward_only:
                accuracy  = accuracy + step_accuracy

        normalized_loss = loss / float(iters)

        # Write summary
        # writer.add_summary(str(normalized_loss), epoch)

        # Print loss and accuracy for the model
        if verbose :
            if not self.forward_only:
                print("Training loss: {0}".format(normalized_loss))
            else:
                normalized_accuracy = accuracy / float(iters)
                print("Test loss: {0}".format(normalized_loss))
                print("Test accuracy: {0}".format(normalized_accuracy))

    def encoder_step(self, session, data, writer, xy, step, epoch, epoch_size,
        start_time, costs, iters, state, verbose=False):

        (x,y) = xy

        # Setup backpropagation
        eval_op = tf.no_op()
        if not self.forward_only:
            eval_op = self.encoder_train_op

        # Encoder data
        feed_dict = {self.input_data: x, self.encoder_targets: y,
            self.initial_state: state}

        # Run model over the batch
        cost, state, _, probabilities, output, logits, encoder_loss_summ = session.run(
            [self.cost, self.encoder_final_state, self.get_encoder_eval_op(),
            self.probabilities, self.output, self.encoder_logits,
            self.encoder_loss_summ],
            feed_dict=feed_dict
        )

        costs += cost
        iters += self.num_steps

        perplexity = np.exp(costs / iters)


        return costs, iters, state, logits

    def encoder_epoch(self, session, epoch, data, writer, verbose=False):
        # // -> integer division
        epoch_size = ((len(data) // self.batch_size) - 1) // self.num_steps
        start_time = time.time()
        costs = 0.0
        iters = 0
        state = self.initial_state.eval()
        for step, (x, y) in enumerate(reader.ptb_iterator(data, self.batch_size,
                                                          self.num_steps)):
            costs, iters, state, logits = self.encoder_step(session, data, writer,
                (x,y), step, epoch, epoch_size, start_time, costs, iters, state, verbose=False)

        # Write summary
        # writer.add_summary(str(np.exp(costs / iters)), (epoch))

        # TODO: return data should be specified
        return np.exp(costs / iters), logits


    def decoder_step(self, session, data, writer, xy, step,  epoch, epoch_size,
        start_time, costs, iters, state, verbose=False):
        # @arg1: fetches (properties to retreive)
        # @arg2: feeds (properties to feed)

        (x,y) = xy

        feed_dict = {self.input_data: x, self.encoder_targets: y}

        cost, state, _, output, logits = session.run(
            [self.decoder_cost, self.decoder_final_state,
            self.get_decoder_eval_op(), self.decoder_output,
            self.decoder_logits],
            feed_dict=feed_dict
        )

        costs += cost
        iters += self.num_steps

        perplexity = np.exp(costs / iters)

        return costs, iters, state, logits

    def decoder_epoch(self, session, epoch, data, writer, verbose=False):
        epoch_size = ((len(data) // self.batch_size) - 1) // self.num_steps
        start_time = time.time()
        costs = 0.0
        iters = 0
        state = self.initial_state.eval()

        for step, (x, y) in enumerate(reader.ptb_iterator(data, self.batch_size,
                                                          self.num_steps)):
            costs, iters, state, logits = self.decoder_step(session, data, writer,
                (x,y), step, epoch, epoch_size, start_time, costs, iters, state, verbose=False)


        # Write summary
        # writer.add_summary(str(np.exp(costs / iters)), (epoch))

        return np.exp(costs / iters), logits

    def get_encoder_eval_op(self):
        eval_op = tf.no_op()
        if not self.forward_only:
            eval_op = self.encoder_train_op
        return eval_op

    def get_decoder_eval_op(self):
        eval_op = tf.no_op()
        if not self.forward_only:
            eval_op = self.decoder_train_op
        return eval_op

    @property
    def decoder_inital_state(self):
        return self._decoder_inital_state

    @property
    def decoder_lr(self):
        return self._decoder_lr

    @property
    def decoder_logits(self):
        return self._decoder_logits

    @property
    def decoder_cell(self):
        return self._decoder_cell

    @property
    def decoder_cost(self):
        return self._decoder_cost

    @property
    def decoder_final_state(self):
        return self._decoder_final_state

    @property
    def decoder_train_op(self):
        return self._decoder_train_op

    @property
    def decoder_output(self):
        return self._decoder_output

    @property
    def input_data(self):
        return self._input_data

    def assign_encoder_lr(self, session, lr_value):
        session.run(tf.assign(self.encoder_lr, lr_value))

    def assign_classifier_lr(self, session, lr_value):
        session.run(tf.assign(self.classifier_lr, lr_value))

    def assign_decoder_lr(self, session, lr_value):
        session.run(tf.assign(self.decoder_lr, lr_value))

    @property
    def summaries(self):
        return self._summaries

    @property
    def saver(self):
        return self._saver

    @property
    def cell(self):
        return self._cell

    @property
    def decoder_inputs(self):
        return self._decoder_inputs

    @property
    def input_data(self):
        return self._input_data

    @property
    def output_probs(self):
      return self._output_probs

    @property
    def encoder_targets(self):
        return self._encoder_targets

    @property
    def initial_state(self):
        return self._initial_state

    @property
    def encoder_logits(self):
        return self._encoder_logits

    @property
    def output(self):
        return self._output

    @property
    def probabilities(self):
        return self._probabilities

    @property
    def decoder_probabilities(self):
        return self._decoder_probabilities

    @property
    def cost(self):
        return self._cost

    @property
    def classifier_final_state(self):
        return self._final_state

    @property
    def encoder_final_state(self):
        return self._encoder_final_state

    @property
    def encoder_lr(self):
        return self._encoder_lr

    @property
    def classifier_lr(self):
        return self._classifier_lr

    @property
    def encoder_train_op(self):
        return self._encoder_train_op
