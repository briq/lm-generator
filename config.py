class SentimentConfig(object):
    """ Sentiment classifier configuration """
    lm_lr         = 1.0
    classifier_lr = 1.0

class SmallConfig(object):
    """Small config."""
    init_scale    = 0.1
    learning_rate = 0.9
    max_grad_norm = 5
    num_layers    = 1
    num_steps     = 50
    hidden_size   = 50
    max_epoch     = 4
    max_max_epoch = 13
    keep_prob     = 1.0
    keep_bias     = 1.0
    lr_decay      = 0.97
    batch_size    = 50
    vocab_size    = 11310
    num_classes   = 2
    beam_size     = 1

class SmallGenConfig(object):
  """Small config. for generation"""
  init_scale = 0.1
  learning_rate = 1.0
  max_grad_norm = 5
  num_layers = 1
  num_steps = 1
  hidden_size = 50
  max_epoch = 4
  max_max_epoch = 13
  keep_prob = 1.0
  keep_bias     = 1.0
  lr_decay = 0.5
  batch_size = 1
  vocab_size = 11310
  num_classes   = 2
  beam_size     = 1

class MediumConfig(object):
    """Medium config."""
    init_scale    = 0.05
    learning_rate = 1.0
    max_grad_norm = 5
    num_layers    = 2
    num_steps     = 50
    hidden_size   = 650
    max_epoch     = 6
    max_max_epoch = 39
    keep_prob     = 1.0
    keep_bias     = 1.0
    lr_decay      = 0.8
    batch_size    = 50
    vocab_size    = 11310
    num_classes   = 2
    beam_size     = 1


class LargeConfig(object):
    """Large config."""
    init_scale    = 0.04
    learning_rate = 1.0
    max_grad_norm = 10
    num_layers    = 2
    num_steps     = 35
    hidden_size   = 1500
    max_epoch     = 14
    max_max_epoch = 55
    keep_prob     = 0.35
    keep_bias     = 1.0
    lr_decay      = 1 / 1.15
    batch_size    = 20
    vocab_size    = 11310
    num_classes   = 2
    beam_size     = 1
