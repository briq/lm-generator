ubuntu@ip-172-31-1-164:~/lm-generator$ export LD_LIBRARY_PATH=/usr/local/cuda-7.5/lib64
ubuntu@ip-172-31-1-164:~/lm-generator$ ./run.sh 
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcublas.so locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcudnn.so locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcufft.so locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcuda.so.1 locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcurand.so locally
Preparing data in data/
I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:900] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
I tensorflow/core/common_runtime/gpu/gpu_init.cc:102] Found device 0 with properties: 
name: GRID K520
major: 3 minor: 0 memoryClockRate (GHz) 0.797
pciBusID 0000:00:03.0
Total memory: 4.00GiB
Free memory: 3.95GiB
I tensorflow/core/common_runtime/gpu/gpu_init.cc:126] DMA: 0 
I tensorflow/core/common_runtime/gpu/gpu_init.cc:136] 0:   Y 
I tensorflow/core/common_runtime/gpu/gpu_device.cc:755] Creating TensorFlow device (/gpu:0) -> (device: 0, name: GRID K520, pci bus id: 0000:00:03.0)
WARNING:tensorflow:Passing a `GraphDef` to the SummaryWriter is deprecated. Pass a `Graph` object instead, such as `sess.graph`.
Created model with fresh parameters.
Training the encoder
Epoch: 1 Learning rate: 1.000
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 4529 get requests, put_count=2823 evicted_count=1000 eviction_rate=0.354233 and unsatisfied allocation rate=0.619563
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 100 to 110
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 4531 get requests, put_count=3880 evicted_count=2000 eviction_rate=0.515464 and unsatisfied allocation rate=0.58795
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 146 to 160
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=2019 evicted_count=2000 eviction_rate=0.990589 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=2028 evicted_count=2000 eviction_rate=0.986193 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 2 get requests, put_count=2040 evicted_count=2000 eviction_rate=0.980392 and unsatisfied allocation rate=1
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 449 to 493
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=1072 evicted_count=1000 eviction_rate=0.932836 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 4531 get requests, put_count=5113 evicted_count=2000 eviction_rate=0.39116 and unsatisfied allocation rate=0.338336
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 1273 to 1400
Epoch: 1 Train Perplexity: 12849.267
Epoch: 1 Valid Perplexity: 12068.894
Epoch: 2 Learning rate: 1.000
Epoch: 2 Train Perplexity: 11307.720
Epoch: 2 Valid Perplexity: 10545.286
Epoch: 3 Learning rate: 1.000
Epoch: 3 Train Perplexity: 9772.496
Epoch: 3 Valid Perplexity: 9034.704
Epoch: 4 Learning rate: 1.000
Epoch: 4 Train Perplexity: 8300.476
Epoch: 4 Valid Perplexity: 7620.946
Epoch: 5 Learning rate: 1.000
Epoch: 5 Train Perplexity: 6988.951
Epoch: 5 Valid Perplexity: 6390.739
Epoch: 6 Learning rate: 1.000
Epoch: 6 Train Perplexity: 5858.833
Epoch: 6 Valid Perplexity: 5364.304
Epoch: 7 Learning rate: 1.000
Epoch: 7 Train Perplexity: 4968.977
Epoch: 7 Valid Perplexity: 4583.014
Epoch: 8 Learning rate: 1.000
Epoch: 8 Train Perplexity: 4268.605
Epoch: 8 Valid Perplexity: 3977.957
Epoch: 9 Learning rate: 1.000
Epoch: 9 Train Perplexity: 3749.955
Epoch: 9 Valid Perplexity: 3524.488
Epoch: 10 Learning rate: 1.000
Epoch: 10 Train Perplexity: 3351.617
Epoch: 10 Valid Perplexity: 3182.272
ubuntu@ip-172-31-1-164:~/lm-generator$ ./run.sh export LD_LIBRARY_PATH=/usr/local/cuda-7.5/lib64[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[Cit[Kr_outputpython test.py[6Pr_output[4Pexitport LD_LIBRARY_PATH=/usr/local/cuda-7.5/lib64[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C./run.sh [K[Knano config.py
(B)0[?1049h[1;30r[m[4l[34h[?25h[?1h=[?1h=[?1h=[39;49m[39;49m[m[H[J[0;7m  GNU nano 2.2.6                                                                   File: config.py                                                                                                                                             [3;1H[0;1m[36mclass[39m[m SentimentConfig(object):[4;5H[0;1m[32m""" Sentiment classifier configuration """[5;5H[39m[mlm_lr[5;19H= 1.0[6;5Hclassifier_lr = 1.0[2B[0;1m[36mclass[39m[m SmallConfig(object):[9;5H[0;1m[32m"""Small config."""[10;5H[39m[minit_scale    = 0.1[11;5Hlearning_rate = 1.0[12;5Hmax_grad_norm = 5[13;5Hnum_layers    = 2[14;5Hnum_steps     = 50[15;5Hhidden_size   = 200[16;5Hmax_epoch     = 4[17;5Hmax_max_epoch = 10[18;5Hkeep_prob     = 0.5[19;5Hkeep_bias     = 1.0[20;5Hlr_decay[6C= 0.97[21;5Hbatch_size    = 50[22;5Hvocab_size    = 12374[23;5Hnum_classes   = 2[24;5Hbeam_size     = 1[3B[0;1m[36mclass[39m[m MediumConfig(object):[28;112H[0;7m[ Read 60 lines ][1B^G[m Get Help[29;40H[0;7m^O[m WriteOut[29;79H[0;7m^R[m Read File[29;118H[0;7m^Y[m Prev Page[29;157H[0;7m^K[m Cut Text[29;196H[0;7m^C[m Cur Pos[1B[0;7m^X[m Exit[30;40H[0;7m^J[m Justify[30;79H[0;7m^W[m Where Is[30;118H[0;7m^V[m Next Page[30;157H[0;7m^U[m UnCut Text[30;196H[0;7m^T[m To Spell[27A[1B[1B[1B[1B[1B[1B[1B[1B[1B[1B[1B[1B[1B[1B[1B[1B[18;24HM[1;230H[0;7mModified[17;21H[m0[K20[11B[0;7mFile Name to Write: config.py                                                                                                                                                                                                                  [29;40H[m           [29;60H[0;7mM-D[m DOS Format                 [29;118H [0;7mM-A[m Append [29;157H           [29;178H[0;7mM-B[m Backup File[K[30;2H[0;7mC[m Cancel[30;40H          [30;60H[0;7mM-M[m Mac Format                [30;118H [0;7mM-P[m Prepend[K[28;30H[1;230H[39;49m[0;7m        [28;110H[m[1K [0;7m[ Wrote 60 lines ][m[K[29;40H[0;7m^O[m WriteOut[29;60H                   [0;7m^R[m Read File[29;118H[0;7m^Y[m Prev Page[29;157H[0;7m^K[m Cut Text[29;178H                  [0;7m^C[m Cur Pos[30;2H[0;7mX[m Exit  [30;40H[0;7m^J[m Justify[30;60H                   [0;7m^W[m Where Is[30;118H[0;7m^V[m Next Page[30;157H[0;7m^U[m UnCut Text[30;196H[0;7m^T[m To Spell[17;22HM[13B[J[30;239H[30;1H[?1049l[?1l>ubuntu@ip-172-31-1-164:~/lm-generator$ nano config.py[5P./run.sh 
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcublas.so locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcudnn.so locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcufft.so locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcuda.so.1 locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcurand.so locally
Preparing data in data/
I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:900] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
I tensorflow/core/common_runtime/gpu/gpu_init.cc:102] Found device 0 with properties: 
name: GRID K520
major: 3 minor: 0 memoryClockRate (GHz) 0.797
pciBusID 0000:00:03.0
Total memory: 4.00GiB
Free memory: 3.95GiB
I tensorflow/core/common_runtime/gpu/gpu_init.cc:126] DMA: 0 
I tensorflow/core/common_runtime/gpu/gpu_init.cc:136] 0:   Y 
I tensorflow/core/common_runtime/gpu/gpu_device.cc:755] Creating TensorFlow device (/gpu:0) -> (device: 0, name: GRID K520, pci bus id: 0000:00:03.0)
WARNING:tensorflow:Passing a `GraphDef` to the SummaryWriter is deprecated. Pass a `Graph` object instead, such as `sess.graph`.
Reading model parameters from data/checkpoints/transformtrain.ckpt-3710
Training the encoder
Epoch: 1 Learning rate: 1.000
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 4529 get requests, put_count=2824 evicted_count=1000 eviction_rate=0.354108 and unsatisfied allocation rate=0.619342
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 100 to 110
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 4531 get requests, put_count=3872 evicted_count=2000 eviction_rate=0.516529 and unsatisfied allocation rate=0.589715
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 146 to 160
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=2021 evicted_count=2000 eviction_rate=0.989609 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=2030 evicted_count=2000 eviction_rate=0.985222 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 4531 get requests, put_count=4265 evicted_count=2000 eviction_rate=0.468933 and unsatisfied allocation rate=0.509821
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 493 to 542
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=1072 evicted_count=1000 eviction_rate=0.932836 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 4531 get requests, put_count=5114 evicted_count=2000 eviction_rate=0.391083 and unsatisfied allocation rate=0.338115
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 1273 to 1400
Epoch: 1 Train Perplexity: 3050.190
Epoch: 1 Valid Perplexity: 2916.848
Epoch: 2 Learning rate: 1.000
Epoch: 2 Train Perplexity: 2802.493
Epoch: 2 Valid Perplexity: 2705.607
Epoch: 3 Learning rate: 1.000
Epoch: 3 Train Perplexity: 2601.928
Epoch: 3 Valid Perplexity: 2511.715
Epoch: 4 Learning rate: 1.000
Epoch: 4 Train Perplexity: 2433.553
Epoch: 4 Valid Perplexity: 2349.024
Epoch: 5 Learning rate: 1.000
Epoch: 5 Train Perplexity: 2288.145
Epoch: 5 Valid Perplexity: 2215.091
Epoch: 6 Learning rate: 1.000
Epoch: 6 Train Perplexity: 2174.605
Epoch: 6 Valid Perplexity: 2117.958
Epoch: 7 Learning rate: 1.000
Epoch: 7 Train Perplexity: 2076.335
Epoch: 7 Valid Perplexity: 2032.262
Epoch: 8 Learning rate: 1.000
Epoch: 8 Train Perplexity: 1988.076
Epoch: 8 Valid Perplexity: 1941.664
Epoch: 9 Learning rate: 1.000
Epoch: 9 Train Perplexity: 1909.623
Epoch: 9 Valid Perplexity: 1866.187
Epoch: 10 Learning rate: 1.000
Epoch: 10 Train Perplexity: 1838.028
Epoch: 10 Valid Perplexity: 1796.031
Epoch: 11 Learning rate: 1.000
Epoch: 11 Train Perplexity: 1765.398
Epoch: 11 Valid Perplexity: 1729.350
Epoch: 12 Learning rate: 1.000
Epoch: 12 Train Perplexity: 1697.473
Epoch: 12 Valid Perplexity: 1657.327
Epoch: 13 Learning rate: 1.000
Epoch: 13 Train Perplexity: 1634.994
Epoch: 13 Valid Perplexity: 1599.486
Epoch: 14 Learning rate: 1.000
Epoch: 14 Train Perplexity: 1578.492
Epoch: 14 Valid Perplexity: 1541.355
Epoch: 15 Learning rate: 1.000
Epoch: 15 Train Perplexity: 1531.070
Epoch: 15 Valid Perplexity: 1498.534
Epoch: 16 Learning rate: 1.000
Epoch: 16 Train Perplexity: 1485.473
Epoch: 16 Valid Perplexity: 1450.364
Epoch: 17 Learning rate: 1.000
Epoch: 17 Train Perplexity: 1439.419
Epoch: 17 Valid Perplexity: 1409.176
Epoch: 18 Learning rate: 1.000
Epoch: 18 Train Perplexity: 1400.991
Epoch: 18 Valid Perplexity: 1374.505
Epoch: 19 Learning rate: 1.000
Epoch: 19 Train Perplexity: 1368.437
Epoch: 19 Valid Perplexity: 1339.616
Epoch: 20 Learning rate: 1.000
Epoch: 20 Train Perplexity: 1334.166
Epoch: 20 Valid Perplexity: 1311.131
ubuntu@ip-172-31-1-164:~/lm-generator$ ./run.sh 
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcublas.so locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcudnn.so locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcufft.so locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcuda.so.1 locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcurand.so locally
Preparing data in data/
I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:900] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
I tensorflow/core/common_runtime/gpu/gpu_init.cc:102] Found device 0 with properties: 
name: GRID K520
major: 3 minor: 0 memoryClockRate (GHz) 0.797
pciBusID 0000:00:03.0
Total memory: 4.00GiB
Free memory: 3.95GiB
I tensorflow/core/common_runtime/gpu/gpu_init.cc:126] DMA: 0 
I tensorflow/core/common_runtime/gpu/gpu_init.cc:136] 0:   Y 
I tensorflow/core/common_runtime/gpu/gpu_device.cc:755] Creating TensorFlow device (/gpu:0) -> (device: 0, name: GRID K520, pci bus id: 0000:00:03.0)
WARNING:tensorflow:Passing a `GraphDef` to the SummaryWriter is deprecated. Pass a `Graph` object instead, such as `sess.graph`.
Reading model parameters from data/checkpoints/transformtrain.ckpt-11130
Training the encoder
Epoch: 1 Learning rate: 1.000
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 4529 get requests, put_count=2824 evicted_count=1000 eviction_rate=0.354108 and unsatisfied allocation rate=0.619342
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 100 to 110
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 4531 get requests, put_count=3880 evicted_count=2000 eviction_rate=0.515464 and unsatisfied allocation rate=0.58795
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 146 to 160
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=2019 evicted_count=2000 eviction_rate=0.990589 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=2028 evicted_count=2000 eviction_rate=0.986193 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=2040 evicted_count=2000 eviction_rate=0.980392 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=1065 evicted_count=1000 eviction_rate=0.938967 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=1105 evicted_count=1000 eviction_rate=0.904977 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 18124 get requests, put_count=17916 evicted_count=1000 eviction_rate=0.055816 and unsatisfied allocation rate=0.0790664
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 2478 to 2725
Epoch: 1 Train Perplexity: 1308.327
Epoch: 1 Valid Perplexity: 1277.463
Epoch: 2 Learning rate: 1.000
Epoch: 2 Train Perplexity: 1276.074
Epoch: 2 Valid Perplexity: 1248.181
Epoch: 3 Learning rate: 1.000
Epoch: 3 Train Perplexity: 1248.329
Epoch: 3 Valid Perplexity: 1216.873
Epoch: 4 Learning rate: 1.000
Epoch: 4 Train Perplexity: 1223.986
Epoch: 4 Valid Perplexity: 1189.234
Epoch: 5 Learning rate: 1.000
Epoch: 5 Train Perplexity: 1191.236
Epoch: 5 Valid Perplexity: 1165.435
Epoch: 6 Learning rate: 1.000
Epoch: 6 Train Perplexity: 1162.019
Epoch: 6 Valid Perplexity: 1125.962
Epoch: 7 Learning rate: 1.000
Epoch: 7 Train Perplexity: 1129.545
Epoch: 7 Valid Perplexity: 1097.200
Epoch: 8 Learning rate: 1.000
Epoch: 8 Train Perplexity: 1101.807
Epoch: 8 Valid Perplexity: 1070.728
Epoch: 9 Learning rate: 1.000
Epoch: 9 Train Perplexity: 1074.829
Epoch: 9 Valid Perplexity: 1045.648
Epoch: 10 Learning rate: 1.000
Epoch: 10 Train Perplexity: 1046.690
Epoch: 10 Valid Perplexity: 1010.659
Epoch: 11 Learning rate: 1.000
Epoch: 11 Train Perplexity: 1009.710
Epoch: 11 Valid Perplexity: 972.065
Epoch: 12 Learning rate: 1.000
Epoch: 12 Train Perplexity: 976.273
Epoch: 12 Valid Perplexity: 939.885
Epoch: 13 Learning rate: 1.000
Epoch: 13 Train Perplexity: 937.498
Epoch: 13 Valid Perplexity: 900.022
Epoch: 14 Learning rate: 1.000
Epoch: 14 Train Perplexity: 902.241
Epoch: 14 Valid Perplexity: 866.653
Epoch: 15 Learning rate: 1.000
Epoch: 15 Train Perplexity: 860.083
Epoch: 15 Valid Perplexity: 816.581
Epoch: 16 Learning rate: 1.000
Epoch: 16 Train Perplexity: 813.223
Epoch: 16 Valid Perplexity: 806.744
Epoch: 17 Learning rate: 1.000
Epoch: 17 Train Perplexity: 810.133
Epoch: 17 Valid Perplexity: 756.469
Epoch: 18 Learning rate: 1.000
Epoch: 18 Train Perplexity: 796.520
Epoch: 18 Valid Perplexity: 868.417
Epoch: 19 Learning rate: 1.000
Epoch: 19 Train Perplexity: 789.291
Epoch: 19 Valid Perplexity: 762.472
Epoch: 20 Learning rate: 1.000
Epoch: 20 Train Perplexity: 779.653
Epoch: 20 Valid Perplexity: 750.105
ubuntu@ip-172-31-1-164:~/lm-generator$ ./run.sh 
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcublas.so locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcudnn.so locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcufft.so locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcuda.so.1 locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcurand.so locally
Preparing data in data/
I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:900] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
I tensorflow/core/common_runtime/gpu/gpu_init.cc:102] Found device 0 with properties: 
name: GRID K520
major: 3 minor: 0 memoryClockRate (GHz) 0.797
pciBusID 0000:00:03.0
Total memory: 4.00GiB
Free memory: 3.95GiB
I tensorflow/core/common_runtime/gpu/gpu_init.cc:126] DMA: 0 
I tensorflow/core/common_runtime/gpu/gpu_init.cc:136] 0:   Y 
I tensorflow/core/common_runtime/gpu/gpu_device.cc:755] Creating TensorFlow device (/gpu:0) -> (device: 0, name: GRID K520, pci bus id: 0000:00:03.0)
WARNING:tensorflow:Passing a `GraphDef` to the SummaryWriter is deprecated. Pass a `Graph` object instead, such as `sess.graph`.
Reading model parameters from data/checkpoints/transformtrain.ckpt-18550
Training the encoder
Epoch: 1 Learning rate: 1.000
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 4529 get requests, put_count=2827 evicted_count=1000 eviction_rate=0.353732 and unsatisfied allocation rate=0.61868
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 100 to 110
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 4531 get requests, put_count=3886 evicted_count=2000 eviction_rate=0.514668 and unsatisfied allocation rate=0.586625
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 146 to 160
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=2019 evicted_count=2000 eviction_rate=0.990589 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=2028 evicted_count=2000 eviction_rate=0.986193 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 4531 get requests, put_count=4213 evicted_count=2000 eviction_rate=0.474721 and unsatisfied allocation rate=0.520415
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 449 to 493
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=1065 evicted_count=1000 eviction_rate=0.938967 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=1105 evicted_count=1000 eviction_rate=0.904977 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 18124 get requests, put_count=17896 evicted_count=1000 eviction_rate=0.0558784 and unsatisfied allocation rate=0.0801699
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 2478 to 2725
Epoch: 1 Train Perplexity: 768.003
Epoch: 1 Valid Perplexity: 750.829
Epoch: 2 Learning rate: 1.000
Epoch: 2 Train Perplexity: 761.681
Epoch: 2 Valid Perplexity: 755.144
Epoch: 3 Learning rate: 1.000
Epoch: 3 Train Perplexity: 750.249
Epoch: 3 Valid Perplexity: 721.718
Epoch: 4 Learning rate: 1.000
Epoch: 4 Train Perplexity: 742.530
Epoch: 4 Valid Perplexity: 712.151
Epoch: 5 Learning rate: 1.000
Epoch: 5 Train Perplexity: 732.079
Epoch: 5 Valid Perplexity: 700.294
Epoch: 6 Learning rate: 1.000
Epoch: 6 Train Perplexity: 720.928
Epoch: 6 Valid Perplexity: 731.939
Epoch: 7 Learning rate: 1.000
Epoch: 7 Train Perplexity: 708.559
Epoch: 7 Valid Perplexity: 682.022
Epoch: 8 Learning rate: 1.000
Epoch: 8 Train Perplexity: 695.165
Epoch: 8 Valid Perplexity: 672.763
Epoch: 9 Learning rate: 1.000
Epoch: 9 Train Perplexity: 682.897
Epoch: 9 Valid Perplexity: 689.352
Epoch: 10 Learning rate: 1.000
Epoch: 10 Train Perplexity: 670.481
Epoch: 10 Valid Perplexity: 653.991
Epoch: 11 Learning rate: 1.000
Epoch: 11 Train Perplexity: 655.074
Epoch: 11 Valid Perplexity: 646.974
Epoch: 12 Learning rate: 1.000
Epoch: 12 Train Perplexity: 640.948
Epoch: 12 Valid Perplexity: 621.622
Epoch: 13 Learning rate: 1.000
Epoch: 13 Train Perplexity: 626.055
Epoch: 13 Valid Perplexity: 603.920
Epoch: 14 Learning rate: 1.000
Epoch: 14 Train Perplexity: 613.666
Epoch: 14 Valid Perplexity: 595.685
Epoch: 15 Learning rate: 1.000
Epoch: 15 Train Perplexity: 599.812
Epoch: 15 Valid Perplexity: 587.556
Epoch: 16 Learning rate: 1.000
Epoch: 16 Train Perplexity: 588.202
Epoch: 16 Valid Perplexity: 572.311
Epoch: 17 Learning rate: 1.000
Epoch: 17 Train Perplexity: 574.078
Epoch: 17 Valid Perplexity: 560.592
Epoch: 18 Learning rate: 1.000
Epoch: 18 Train Perplexity: 561.997
Epoch: 18 Valid Perplexity: 548.581
Epoch: 19 Learning rate: 1.000
Epoch: 19 Train Perplexity: 550.421
Epoch: 19 Valid Perplexity: 557.615
Epoch: 20 Learning rate: 1.000
Epoch: 20 Train Perplexity: 539.672
Epoch: 20 Valid Perplexity: 531.913
ubuntu@ip-172-31-1-164:~/lm-generator$ ./run.sh [K./run.sh 
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcublas.so locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcudnn.so locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcufft.so locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcuda.so.1 locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcurand.so locally
Preparing data in data/
I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:900] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
I tensorflow/core/common_runtime/gpu/gpu_init.cc:102] Found device 0 with properties: 
name: GRID K520
major: 3 minor: 0 memoryClockRate (GHz) 0.797
pciBusID 0000:00:03.0
Total memory: 4.00GiB
Free memory: 3.95GiB
I tensorflow/core/common_runtime/gpu/gpu_init.cc:126] DMA: 0 
I tensorflow/core/common_runtime/gpu/gpu_init.cc:136] 0:   Y 
I tensorflow/core/common_runtime/gpu/gpu_device.cc:755] Creating TensorFlow device (/gpu:0) -> (device: 0, name: GRID K520, pci bus id: 0000:00:03.0)
WARNING:tensorflow:Passing a `GraphDef` to the SummaryWriter is deprecated. Pass a `Graph` object instead, such as `sess.graph`.
Reading model parameters from data/checkpoints/transformtrain.ckpt-25970
Training the encoder
Epoch: 1 Learning rate: 1.000
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 4529 get requests, put_count=2829 evicted_count=1000 eviction_rate=0.353482 and unsatisfied allocation rate=0.618238
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 100 to 110
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 4531 get requests, put_count=3864 evicted_count=2000 eviction_rate=0.517598 and unsatisfied allocation rate=0.591481
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 146 to 160
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=2019 evicted_count=2000 eviction_rate=0.990589 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=2028 evicted_count=2000 eviction_rate=0.986193 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 4531 get requests, put_count=4202 evicted_count=2000 eviction_rate=0.475964 and unsatisfied allocation rate=0.522843
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 449 to 493
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=1065 evicted_count=1000 eviction_rate=0.938967 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=1105 evicted_count=1000 eviction_rate=0.904977 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 18124 get requests, put_count=17915 evicted_count=1000 eviction_rate=0.0558191 and unsatisfied allocation rate=0.0791216
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 2478 to 2725
Epoch: 1 Train Perplexity: 529.436
Epoch: 1 Valid Perplexity: 518.188
Epoch: 2 Learning rate: 1.000
Epoch: 2 Train Perplexity: 519.314
Epoch: 2 Valid Perplexity: 518.436
Epoch: 3 Learning rate: 1.000
Epoch: 3 Train Perplexity: 509.676
Epoch: 3 Valid Perplexity: 518.674
Epoch: 4 Learning rate: 1.000
Epoch: 4 Train Perplexity: 498.601
Epoch: 4 Valid Perplexity: 501.492
Epoch: 5 Learning rate: 1.000
Epoch: 5 Train Perplexity: 490.904
Epoch: 5 Valid Perplexity: 498.747
Epoch: 6 Learning rate: 1.000
Epoch: 6 Train Perplexity: 481.750
Epoch: 6 Valid Perplexity: 494.482
Epoch: 7 Learning rate: 1.000
Epoch: 7 Train Perplexity: 473.341
Epoch: 7 Valid Perplexity: 488.952
Epoch: 8 Learning rate: 1.000
Epoch: 8 Train Perplexity: 467.094
Epoch: 8 Valid Perplexity: 470.940
Epoch: 9 Learning rate: 1.000
Epoch: 9 Train Perplexity: 459.080
Epoch: 9 Valid Perplexity: 476.855
Epoch: 10 Learning rate: 1.000
Epoch: 10 Train Perplexity: 452.921
Epoch: 10 Valid Perplexity: 460.922
Epoch: 11 Learning rate: 1.000
Epoch: 11 Train Perplexity: 445.319
Epoch: 11 Valid Perplexity: 457.873
Epoch: 12 Learning rate: 1.000
Epoch: 12 Train Perplexity: 441.100
Epoch: 12 Valid Perplexity: 451.544
Epoch: 13 Learning rate: 1.000
Epoch: 13 Train Perplexity: 434.098
Epoch: 13 Valid Perplexity: 450.646
Epoch: 14 Learning rate: 1.000
Epoch: 14 Train Perplexity: 427.755
Epoch: 14 Valid Perplexity: 436.231
Epoch: 15 Learning rate: 1.000
Epoch: 15 Train Perplexity: 422.828
Epoch: 15 Valid Perplexity: 436.229
Epoch: 16 Learning rate: 1.000
Epoch: 16 Train Perplexity: 416.725
Epoch: 16 Valid Perplexity: 439.773
Epoch: 17 Learning rate: 1.000
Epoch: 17 Train Perplexity: 413.253
Epoch: 17 Valid Perplexity: 422.690
Epoch: 18 Learning rate: 1.000
Epoch: 18 Train Perplexity: 406.174
Epoch: 18 Valid Perplexity: 426.872
Epoch: 19 Learning rate: 1.000
Epoch: 19 Train Perplexity: 401.478
Epoch: 19 Valid Perplexity: 418.836
Epoch: 20 Learning rate: 1.000
Epoch: 20 Train Perplexity: 397.254
Epoch: 20 Valid Perplexity: 426.933
ubuntu@ip-172-31-1-164:~/lm-generator$ ./run.sh 
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcublas.so locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcudnn.so locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcufft.so locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcuda.so.1 locally
I tensorflow/stream_executor/dso_loader.cc:105] successfully opened CUDA library libcurand.so locally
Preparing data in data/
I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:900] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
I tensorflow/core/common_runtime/gpu/gpu_init.cc:102] Found device 0 with properties: 
name: GRID K520
major: 3 minor: 0 memoryClockRate (GHz) 0.797
pciBusID 0000:00:03.0
Total memory: 4.00GiB
Free memory: 3.95GiB
I tensorflow/core/common_runtime/gpu/gpu_init.cc:126] DMA: 0 
I tensorflow/core/common_runtime/gpu/gpu_init.cc:136] 0:   Y 
I tensorflow/core/common_runtime/gpu/gpu_device.cc:755] Creating TensorFlow device (/gpu:0) -> (device: 0, name: GRID K520, pci bus id: 0000:00:03.0)
WARNING:tensorflow:Passing a `GraphDef` to the SummaryWriter is deprecated. Pass a `Graph` object instead, such as `sess.graph`.
Reading model parameters from data/checkpoints/transformtrain.ckpt-33390
Training the encoder
Epoch: 1 Learning rate: 1.000
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 4529 get requests, put_count=2834 evicted_count=1000 eviction_rate=0.352858 and unsatisfied allocation rate=0.617134
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 100 to 110
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 4531 get requests, put_count=3890 evicted_count=2000 eviction_rate=0.514139 and unsatisfied allocation rate=0.585743
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 146 to 160
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=2019 evicted_count=2000 eviction_rate=0.990589 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=2028 evicted_count=2000 eviction_rate=0.986193 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 4531 get requests, put_count=4220 evicted_count=2000 eviction_rate=0.473934 and unsatisfied allocation rate=0.51887
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 449 to 493
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=1065 evicted_count=1000 eviction_rate=0.938967 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 0 get requests, put_count=1105 evicted_count=1000 eviction_rate=0.904977 and unsatisfied allocation rate=0
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:244] PoolAllocator: After 18124 get requests, put_count=17907 evicted_count=1000 eviction_rate=0.0558441 and unsatisfied allocation rate=0.079563
I tensorflow/core/common_runtime/gpu/pool_allocator.cc:256] Raising pool_size_limit_ from 2478 to 2725
Epoch: 1 Train Perplexity: 393.033
Epoch: 1 Valid Perplexity: 412.141
Epoch: 2 Learning rate: 1.000
Epoch: 2 Train Perplexity: 389.249
Epoch: 2 Valid Perplexity: 412.124
Epoch: 3 Learning rate: 1.000
Epoch: 3 Train Perplexity: 384.949
Epoch: 3 Valid Perplexity: 428.902
Epoch: 4 Learning rate: 1.000
Epoch: 4 Train Perplexity: 380.726
Epoch: 4 Valid Perplexity: 398.365
Epoch: 5 Learning rate: 1.000
Epoch: 5 Train Perplexity: 375.340
Epoch: 5 Valid Perplexity: 393.455
Epoch: 6 Learning rate: 1.000
Epoch: 6 Train Perplexity: 371.915
Epoch: 6 Valid Perplexity: 408.330
Epoch: 7 Learning rate: 1.000
Epoch: 7 Train Perplexity: 368.897
Epoch: 7 Valid Perplexity: 403.477
Epoch: 8 Learning rate: 1.000
Epoch: 8 Train Perplexity: 365.222
Epoch: 8 Valid Perplexity: 385.445
Epoch: 9 Learning rate: 1.000
Epoch: 9 Train Perplexity: 361.413
Epoch: 9 Valid Perplexity: 382.153
Epoch: 10 Learning rate: 1.000
Epoch: 10 Train Perplexity: 357.890
Epoch: 10 Valid Perplexity: 381.688
Epoch: 11 Learning rate: 1.000
Epoch: 11 Train Perplexity: 354.388
Epoch: 11 Valid Perplexity: 380.836
Epoch: 12 Learning rate: 1.000
Epoch: 12 Train Perplexity: 350.957
Epoch: 12 Valid Perplexity: 381.558
Epoch: 13 Learning rate: 1.000
Epoch: 13 Train Perplexity: 348.147
Epoch: 13 Valid Perplexity: 374.437
Epoch: 14 Learning rate: 1.000
Epoch: 14 Train Perplexity: 344.568
Epoch: 14 Valid Perplexity: 367.161
Epoch: 15 Learning rate: 1.000
Epoch: 15 Train Perplexity: 341.937
Epoch: 15 Valid Perplexity: 379.469
Epoch: 16 Learning rate: 1.000
Epoch: 16 Train Perplexity: 339.737
Epoch: 16 Valid Perplexity: 373.170
Epoch: 17 Learning rate: 1.000
^CTraceback (most recent call last):
  File "train.py", line 380, in <module>
    tf.app.run()
  File "/usr/local/lib/python2.7/dist-packages/tensorflow/python/platform/app.py", line 30, in run
    sys.exit(main(sys.argv))
  File "train.py", line 302, in main
    train(VOCABULARY)
  File "train.py", line 163, in train
    encoder_train_data, encoder_valid_data, encoder_test_data, writer)
  File "train.py", line 267, in train_encoder
    verbose=True)
  File "/home/ubuntu/lm-generator/models/transform.py", line 571, in encoder_epoch
    (x,y), step, epoch, epoch_size, start_time, costs, iters, state, verbose=False)
  File "/home/ubuntu/lm-generator/models/transform.py", line 549, in encoder_step
    feed_dict=feed_dict
  File "/usr/local/lib/python2.7/dist-packages/tensorflow/python/client/session.py", line 340, in run
    run_metadata_ptr)
  File "/usr/local/lib/python2.7/dist-packages/tensorflow/python/client/session.py", line 564, in _run
    feed_dict_string, options, run_metadata)
  File "/usr/local/lib/python2.7/dist-packages/tensorflow/python/client/session.py", line 637, in _do_run
    target_list, options, run_metadata)
  File "/usr/local/lib/python2.7/dist-packages/tensorflow/python/client/session.py", line 644, in _do_call
    return fn(*args)
  File "/usr/local/lib/python2.7/dist-packages/tensorflow/python/client/session.py", line 628, in _run_fn
    session, None, feed_dict, fetch_list, target_list, None)
KeyboardInterrupt
ubuntu@ip-172-31-1-164:~/lm-generator$ ./run.sh nano config.py[5P./run.sh [K./[K[Kls
MMD.ipynb       checkpoint  config.pyc  env        gen.txt       generator.pyc    input.npy            mmd.py   [0m[01;34mmodels[0m      [01;32mrun.sh[0m       test.py  train.py
Untitled.ipynb  config.py   [01;34mdata[0m        error.log  generator.py  hyperparams.npy  [01;31mlm-generator.tar.gz[0m  mmd.pyc  output.npy  screenlog.0  [01;34mthesis[0m   [01;34mutil[0m
ubuntu@ip-172-31-1-164:~/lm-generator$ cd ..
ubuntu@ip-172-31-1-164:~$ exit
exit
