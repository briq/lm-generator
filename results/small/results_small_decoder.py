import matplotlib.pyplot as plt

train_data = [13084.937,13087.498,12420.170,11356.155,11009.004,
    4925.398,1369.938,697.387,513.039,388.060,311.923,259.490,224.337,
    199.274,180.028,165.230,152.933,143.164,134.574]

valid_data = [13087.135,12901.099,11769.244,11716.855,10454.159,3798.274,
    964.893,936.206,1058.040,1331.274,1244.667,1327.756,
    1402.389,1365.850,1725.587,1570.462,
    1535.640,1616.092,1700.150]


plt.ylabel('Perplexity')
plt.xlabel('Epochs')

plt.scatter(range(len(valid_data)),valid_data, c='r')
plt.scatter(range(len(train_data)),train_data, c='b')

plt_valid, = plt.plot(range(len(valid_data)), valid_data, c='r')
plt_train, = plt.plot(range(len(train_data)), train_data, c='b')

plt.legend([plt_valid, plt_train], ['Validation','Training'])

plt.show()
