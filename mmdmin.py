#%matplotlib inline
import random
import numpy as np
from scipy import sparse
import tensorflow as tf
import matplotlib.pyplot as plt

sess = tf.InteractiveSession()

dim = 2
n = 200
m1 = [0,0]
m2 = [5,0]
cov = [[1,0],[0,1]]

x1 = np.random.multivariate_normal(m1, cov, n)
x2 = np.random.multivariate_normal(m2, cov, n)
x = x1[0:1,:]

def pdist2sq(X,Y):
    C = -2*tf.matmul(X,tf.transpose(Y))
    nx = tf.reduce_sum(tf.square(X),1,keep_dims=True)
    ny = tf.reduce_sum(tf.square(Y),1,keep_dims=True)
    D = (C + tf.transpose(ny)) + nx
    return D

def witness(Xs,Xt,z,sig):
    Ds = pdist2sq(Xs,z)
    Dt = pdist2sq(Xt,z)
    Ks = tf.exp(-Ds/tf.square(sig))
    Kt = tf.exp(-Dt/tf.square(sig))

    m = tf.to_float(tf.shape(Xs)[0])
    n = tf.to_float(tf.shape(Xt)[0])

    w = tf.reduce_sum(Ks)/m-tf.reduce_sum(Kt)/n
    return w, Dt

Xs_ = tf.placeholder("float", shape=[None,dim], name='Xs')
Xt_ = tf.placeholder("float", shape=[None,dim], name='Xt')
x_  = tf.placeholder("float", shape=[1,dim], name='x')
l_  = tf.placeholder("float", name='lambda')
d_  = tf.Variable(tf.random_normal([1,dim], stddev=0.01))
z_  = x_ + d_
s_ = tf.placeholder("float")
w_, D_ = witness(Xs_, Xt_, z_, s_)
loss = w_ + l_*tf.reduce_sum(tf.square(d_))

step = tf.train.RMSPropOptimizer(0.1,0.1).minimize(loss)

sess.run(tf.initialize_all_variables())

sigma = 100.0
lambd = 0.0001
winit, Dinit  = sess.run([w_, D_], feed_dict={x_: x, Xs_: x1, Xt_: x2, s_: sigma, l_: lambd})
print 'Witness before training: %.4f' % winit
print 'Distance before training: %.4f' % np.mean(Dinit)

wis = []
for i in range(1,100):
    (s,wi,Di) = sess.run([step,w_,D_], feed_dict={x_: x, Xs_: x1, Xt_: x2, s_: sigma, l_: lambd})
    wis.append(wi)

d = sess.run(d_)
z = x+d

print 'Old point: ' + str(x)
print 'New point: ' + str(z)
#print sess.run(Dt, feed_dict={x_: x, Xs_: x1, Xt_: x2})

wmean,Dmean = sess.run(witness(Xs_,Xt_,z_,10.0),feed_dict={z_:[m2], Xs_: x1, Xt_: x2, s_: sigma, l_: lambd})
print 'Witness after training: %.4f' % wi
print 'Distance after training: %.4f' % np.mean(Di)

print 'Witness for m2: ' + str(wmean)
print 'Distance for m2: ' + str(np.mean(Dmean))

plt.plot(wis)
plt.show()

plt.scatter(x1[:,0],x1[:,1],color=[1,0.8,0.8])
plt.scatter(x2[:,0],x2[:,1],color=[0.8,0.8,1])
plt.scatter(x[0,0],x[0,1],color='r')
plt.scatter(z[0,0],z[0,1],color='b')
plt.plot([x[0,0],z[0,0]],[x[0,1],z[0,1]],color='b')
plt.show()
